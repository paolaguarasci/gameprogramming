# gameProgramming



 [Super Mr Beans - GDD](doc/)





# Lezioni 1 e 2

 - Introduzione a Unity e al Game Developing in generale

# Lezione 3

- Coordinate 3D
- Creazione di una scena con oggetti e luci
- Inserire un player nella scena
- Creare uno script che muove l'oggetto
- Controlli FPS (mouseLock script)

# Lezione 4

- Keyboard input
- Fare fuoco
- Reagire al colpo

# Lezione 5

- AI basica
- Spawn di oggetti a runtime


# Lezione 6

- Whiteboxing
- Immagini 2D
- Skybox
- Modelli 3D custom
- Sistema particellare


# Lezione 7

- Sparare (instanziando oggetti)
- Creare il prefab di un proiettile
- Danni al player
- Migliorare il prefab con i particellari


# Lezione 8

- Colliders
- RigidBody
- Joints


# Lezione 9

- Luce
- Camera


# Lezione 10

- UI components


# Lezione 11

- Auto Layout
- FPS Counter
- Health bar


# Lezione 12

- 



# Lezione 13

- Altri eventi del mouse


# Lezione 14

- 2D Memory Game

# Lezione 15

- Movimenti in terza persona

# Lezione  16

- Applicare e controlare animazioni

# Lezione 17

- Porte apribili
- Oggetti interattivi

# Lezione 18

- Collezionare oggetti

# Lezione 19

- HTTP request

# Lezione 20

- Audio

# Lezione 21

- Unity Navigation System
- Terrain

# 
