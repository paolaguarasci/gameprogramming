# Super Mr. Bean - GDD - Paola Guarasci (mat 189515)

![](img/titoloaCapo.png)

# Panoramica

## Genere

Il gioco è un esponente del genere **Platform**, con elementi di tipo **Arcade**. I platform sono caratterizzati dal loro uso intenso del salto e dell'arrampicata per navigare nell'ambiente del giocatore e raggiungere il loro obiettivo.  I livelli e gli ambienti tendono a presentare terreni irregolari e piattaforme sospese di altezza variabile che richiedono l'uso delle abilità del personaggio del giocatore per attraversare. L'elemento unificante più comune dei giochi di questo genere è la capacità di saltare, appunto, e il nostro eroe, **SuperMrBean** è in grado di saltare e usare il salto come arma. L’elemento *arcade* risiede nella classifica **globale** a fine livello, implementata come servizio di rete, e quindi nel fine ultimo del gioco stesso, *fare punti*. Altro elemento tipico dei giochi arcade è la modalità a tempo. 

## Influenze esterne

### Super Mario 64

<img src="https://upload.wikimedia.org/wikipedia/en/6/6a/Super_Mario_64_box_cover.jpg" style="zoom:150%;" />

Super Mario 64 è un gioco di piattaforma del 1996 per Nintendo 64 e il primo della serie Super Mario a presentare un gameplay 3D. Nei panni di Mario, il giocatore esplora il castello della principessa Peach e deve salvarla da Bowser.

Il giocatore controlla Mario attraverso diversi livelli, ogni livello è un mondo chiuso in cui il giocatore è libero di vagare in tutte le direzioni e scoprire l'ambiente senza limiti di tempo. I mondi sono pieni di nemici che attaccano Mario, così come di creature amichevoli che forniscono assistenza, offrono informazioni o chiedono un favore (come i pacifici amici Bob-ombe rosa). Il giocatore raccoglie stelle in ogni livell, alcune stelle compaiono solo dopo aver completato determinate attività, spesso indicate dal nome del corso. Queste sfide includono sconfiggere un boss, risolvere enigmi, correre contro un avversario e raccogliere monete. Man mano che vengono raccolte più stelle, più aree del mondo del castello diventano accessibili. Il giocatore sblocca le porte del castello con le chiavi ottenute sconfiggendo Bowser in percorsi speciali. Ci sono molti mini-corsi nascosti e altri segreti, la maggior parte dei quali contiene stelle extra richieste per il completo completamento del gioco.

In **SuperMrBeans** il giocatore e’ libero di esplorare il livello, uccidendo i nemici e raccogliendo le monete. Esiste una stella nascosta in un forziere che se trovata e raccolta ricompensa il giocacore con 3000pts. Raccolto il Target finisce il livello. 

###  Spyro the Dragon

<img src="https://upload.wikimedia.org/wikipedia/en/5/53/Spyro_the_Dragon.jpg" style="zoom:150%;" />

Spyro the Dragon è un gioco di piattaforma sviluppato da Insomniac Games e pubblicato da Sony Computer Entertainment per PlayStation il 10 settembre 1998. Il giocatore controlla il personaggio titolare, Spyro. I livelli sono a tempo indeterminato e ruotano attorno all'esplorazione e all'ottenimento di vari oggetti collezionabili per progredire nel gioco. Trovare ogni oggetto da collezione nel gioco sblocca un mondo aggiuntivo a cui altrimenti non sarebbe possibile accedere.

### Piattaforme di riferimento 

Unity3D e’ un ambiente di sviluppo che permette il deploy su molte piattaforme:

- Windows (32bits), Windows (64bits), Linux e MacOS
- iOS e Android
- Web (tecnologia WebGL)
- Playstation e XBOX

Attualmente il progetto ha build funzionanti per sistemi **Windows (64bits)**, **WebGL** e **Linux**. Ci siamo in oltre avvalsi del supporto al build in cloud offerto dalla piattaforma di sviluppo che ci ha fornito la possibilità di buildare in maniera **continua**. 

## Descrizione del progetto

### Ambientazione

Il nostro eroe, Super Mr Beans, è un *fagiolo*, ovvero un *Phaseolus vulgaris* appartenente alla famiglia delle *Fabaceae*. Ma non e’ un semplice *legume*, bensì è un *legume baffuto col sombrero* capace di saltare e di andare in esplorazione immerso in un languido tramonto. E, come se tutto questo non bastasse, deve anche combattere draghi, condor e malefiche galline.

![](img/screen01.png)

### Meccaniche di gioco

1. **Movimento** Il movimento e’ possibile grazie alla pressione dei tasti WASD ed e’ relativo alla camera.
2. **Salto** Abilita del personaggio che gli permette di esplorare il mondo di gioco e di uccidere nemici. 
3. **Morte** Il giocatore ha 5 vite, ogni contatto con nemici provoca una perdita di una vita. 
4. **Raccolta di oggetti** I collezionabili sono monete e stelle. Sono sparsi per il mondo e la loro raccolta conferisce punti.
5. **Interazione** Il tasto E permette l’interazione con oggetti. Es. apertura di casse.
6. **Accumulare punti** Il gioco offre diversi modi per raccogliere punti:
   1. Uccidendo nemici: 200, 300 e 400 pts in base al tipo di nemico.
   2. Raccogliendo monete: 10pts per ogni moneta.
   3. Scovando e raccogliendo la/le stelle: In ogni livello sono nascoste da 1 a 3 stelle, ogni stella raccolta conferisce 3000pts.
7. **Obiettivo del gioco** Accumulare punti. Il livello termina quando si raccoglie il **target**.

![](img/target.PNG)

### Nemici

I nemici sono tre ed ognuno dei tre ha caratteristiche offensive diverse. 

- **Chicken** *gittata* 10 *danno* 1 *colore particellare*  giallo
- **Condor** *gittata* 15 *danno* 2 *colore particellare* verde
- **Dragon** *gittata* 20 *danno* 3 *colore particellare* blu

In tutti e tre i casi la morte del nemico si determina usando il **salto** sopra di essi, per tre volte. 

### Scena Iniziale

![](img/introv3.png)

La scena iniziale offre al giocatore la possibilità di inserire il proprio nome (da usare poi per la classifica) e di avviare il gioco vero e proprio. Alla pressione del tasto SPAZIO si avvia la scena con il livello.

### Livello

![](img/livello.png)

Il gioco e’ costituito da un unico livello. Il personaggio si trova su una spiaggia *lowpoly* in mezzo ad un oceano *lowpoly* e alberi e nemici, tutti *lowpoly*. Sullo sfondo un tramonto e, in lontananza, sulla skyline, alcune isolette. Il livello e’ composto da diverse piattaforme, elementi di scena e nemici. E’ strutturato come un unico piano su cui il giocatore deve muoversi, salire e saltare per poter raccogliere tutti i collezionabili. Deve in oltre fare attenzione ai nemici:

- Condor 
- Dragon
- Chicken

L’esplorazione è libera ed il livello termina in maniera naturale raggiungendo il traguardo. Terminazioni anzitempo avvengono per morte del personaggio. La morte sopraggiunge se si cade in acqua o se si esauriscono le vite. Ogni contato con un nemico provoca la perdita di una vita. 

L’acqua presente in game e’ animata con onde e vento. E’ stata creata usando il plugin LowPoly Water disponibile gratuitamente sullo store ufficiale.

### Classifica

![](img/classifica.png)

La schermata finale mostra il risultato ottenuto dal giocatore e la classifica globale. La classifica è implementata usando un servizio di rete. E’ stato creato un server di gioco, usando Node.JS, Express e MondoDB (come DaaS) che espone le seguenti API:

- POST save 
- GET all
- GET best
- GET latest

La classifica finale attualmente restituisce l’output dell’endpoint **GET best**, ovvero la classifica dei 10 migliori risultati di sempre, ordinati per score, in ordine inverso.

Il servizio e’ online sulla piattaforma Heroku al seguente indirizzo https://tranquil-reaches-15173.herokuapp.com

POST save accetta JSON nel seguente formato:

```json
{
  "timestamp": "12/08/2020 15:55:57",
  "name": "Paola",
  "score": 1100
}
```

Esempio di output di GET best

```json
[
  {
    "_id": "5f37b6d73e4f230017a3974e",
    "timestamp": "01/01/1970 11:11:11",
    "name": "PingFlood",
    "score": 10000000
  },
  {
    "_id": "5f37ba7c9c682a00176b4a50",
    "timestamp": "15/08/2020 10:35:39",
    "name": "paola",
    "score": 24380
  },
  {
    "_id": "5f3723e789b4a603f112df0c",
    "timestamp": "14/08/2020 23:53:11",
    "name": "Paola",
    "score": 22700
  },
  {
    "_id": "5f37b9659c682a00176b4a4f",
    "timestamp": "15/08/2020 10:31:01",
    "name": "",
    "score": 19150
  },
  {
    "_id": "5f37f87455dcd20017003ad0",
    "timestamp": "15/08/2020 15:00:03",
    "name": "Paola",
    "score": 19080
  },
  {
    "_id": "5f37fc5e55dcd20017003ad4",
    "timestamp": "15/08/2020 15:16:45",
    "name": "ssssss",
    "score": 12690
  },
  {
    "_id": "5f37200a88084d01bb41f045",
    "timestamp": "14/08/2020 23:36:42",
    "name": "Paola",
    "score": 10360
  },
  {
    "_id": "5f37bb9b9c682a00176b4a51",
    "timestamp": "8/15/2020 10:40:27 AM",
    "name": "pppp",
    "score": 10190
  },
  {
    "_id": "5f37a40527976902c54e487e",
    "timestamp": "8/15/2020 8:59:49 AM",
    "name": "paola",
    "score": 8260
  },
  {
    "_id": "5f372a773a5e0e03c41538b7",
    "timestamp": "15/08/2020 00:21:11",
    "name": "Paola",
    "score": 7950
  }
]

```

Il Json di risposta è stato deserializzato usando il plugin **json.net** disponibile gratuitamente sullo store ufficiale. 

### Menu di pausa

![](img/pausa.png)

Si accede al menu di pausa premendo il tasto *ESC*. Il menu offre la possibilità di:

- Uscire dal gioco

- Abbassare/Alzare il volume di musica e fx

- Abbassare/Alzare la luminosità

  

## Implementazione

Il corretto funzionamento dell’implementazione del progetto è garantito da numerosi scripts che interagiscono fra di loro ed hanno dei compiti ben precisi.

### Camera

![](img/classi/camera.png)

- **OrbitCamera**: ad ogni update (su LateUpdate), aggiorna la posizione della camera rispetto alla posizione di un target (in questo caso il player).

### Player

![](img/classi/player.png)



Il player è gestito in larga parte dagli script **RelativeMovement**, **PlayerCharacter** e **DeviceOperator**. Per questi script si e’ partiti dalla implementazione vista a lezione e successivamente li si e’ adatti alle esigenze specifiche del gioco. 

**Relative Movement** si occupa del movimento *relativo* alla camera. Riceve in input un GameObject (il target, la camera) e altri parametri di cofigurazione. 

**PlayerCharacter** reagisce agli urti e ai danni (comunicando con PlayerManager) 

**Score** è una classe data usata nella deserializzazione del JSON dei punteggi.

**DeviceOperator** definisce la risponde alla pressione del tasto E mandando un messaggio del tipo *Operate* ai collider che si sovrappongono al nostro giocatore e che sono nella direzione *forward*. Si avvia quindi il metodo Operate, se presente in qualche classe attaccata all’oggetto in collisione.



### Nemici

![](img/classi/enemy.png)

I nemici usano una IA che ne gestisce i movimenti, il fuoco e l’inseguimento del player.

**GallinaAI** definisce il seguente comportamento di base: 

- spostamento lungo una distanza *x* in direzione *avanti* 
-  rotazione di 180

se il player si trova ad una distanza inferiore a 0.75f il personaggio si pone in modalità inseguimento, interrompe la sua routine di base, si volta verso player e si lancia all’inseguimento del player stesso. In questa fase inizia anche a sparare *Fireball*.

**Fireball** script che gestisce i proiettili dei nemici. Ha un metodo *OnTriggerEnter* che si attiva quando l’oggetto cui e’ attaccato collide con un altro oggetto sulla scena. Se *other*, l’altro colider in gioco, e’ il player allora attiva il metodo *Hurt(damage)* che sottrae al player il quantitativo di danni previsti per quella istanza di Fireball.

**ReactiveTarget** richiamato dal gestore del salto del player quando si verifica la collisione. Reagisce alla collisione e distrugge il GameObject.



### Enviroment

![](img/classi/env.png)

**Spin** script per la rotazione sull’asse Y

**CollectableItem** reagisce alla triggerata da parte di un altro collider e se questo altro collider appartiene al player inserisce l’item nel inventario e lo distrugge.

**OpenChest** interagisce con l’Animator del *Chest* per triggerare l’animazione dell’apertura del baule. 



### Manager, Controler  & co.



Il gioco utilizza un sistema persistente di Manager. Esiste una scena definita **Boot** che ha un un unico GameObject: il **GameManager**. Nel GameManager, il manager principale, avviene il caricamento a runtime degli altri manager (gestiti come prefabs e instanziati). Avviene anche il caricamento delle scene, in modalità *additiva*. La scena *Boot* è la prima ad essere caricata, resta tale per tutta la durata dell’esecuzione ed e’ l’ultima ad essere rimossa. Ogni manager, inoltre, e’ un Singleton, per cui eredita da una classe **Singleton** appositamente creata che a sua volta eredita da *MonoBehaviour*, per far si che in ogni caso siano disponibili i metodi e le proprietà della classe *MonoBehaviour*. I manager tra di loro comunicano utilizzando gli eventi. E’ stata creata per questo una classe **Events** che tiene traccia delle varie tipologie di eventi codificati.  

![managerAlberoEspanso](/img/classi\managerAlberoEspanso.png)

**UI Manager** responsabile del caricamento del menu iniziale, del menu di pausa, del game over.

**HUD Manager** visualizza e aggiorna le informazioni a schermo su vita e punteggi 

![](/img/monete.PNG) ![](/img/vite.png)

**AudioManager** gestisce la musica del livello ed espone metodi per alza/abbassare il volume. 

**LightManager** gestisce l’illuminazione in gioco. Espone metodi per dimmerare la luminosità.

**PlayerManager** gestisce vita e punteggio del player

**InventoryManager** gestisce l’inventario e operazioni CRUD su di esso.

**LeaderboardManager** gestisce la classifica dei punteggi. Permette invio del singolo punteggio e ricezione della classifica in formato JSON (di cui fa la deserializzazione con *json.net*)

**NetworkService** gestisce i servizi di rete. Ha un metodo privato *CallAPI* e metodi pubblici specializzati che, sotto il cofano, lo richiamano. CallAPI permette di effettuare chiamate POST e GET. Lo switch e’ automatico: lo script si basa sul semplice assunto per cui se sono presenti argomenti e’ una post, e gli argomenti sono inviati nel tipo www-form-data.

 ![](img/classi/managerOther.png)



**MainMenu** gestione della scena iniziale. Imposta il nome del player usando la specifica *PlayerSetting* che permette di conservare settaggi tra i cambi scena e, cosa più importante, tra una partita e l’altra. E’ quindi possibile mantenere il nome impostato anche nelle successive esecuzioni del gioco. Imposta anche un cursore personalizzato che rimarrà tale per tutta la durata del gioco. 

**PauseMenu** gestisce il menu di pausa. Espone metodi sottoscritti dai bottoni della UI, come ad esempio OnLuminositaMeno che reagisce al click sul bottone relativo e setta, tramite il manger della luminosità, un valore inferiore di 0.25f.



![Controller](img/classi/menu.png)



## Assets

Tutto il tema del gioco e’ **low poly**. Sono stati usati tutti assets che richiamano questa corrente artistica e, più in generale, e’ stata compiuta una scelta stilisca cartoonesca. Tutti gli assets, ad esclusione di quelli di Kenney, sono liberamente scaricabili dall’Assets Store di Unity. Gli Assets di Kenney sono disponibili [qui](https://www.kenney.nl/) 

### UI

- [Clean Vector Icons](https://assetstore.unity.com/packages/2d/gui/icons/clean-vector-icons-132084)
- [Pixel Cursors](https://assetstore.unity.com/packages/2d/gui/icons/pixel-cursors-109256)
- [Kenney Fonts Package](https://kenney.nl/assets/kenney-fonts)
- [Kenney UI Pack](https://kenney.nl/assets/ui-pack)
- [Kenney Interface Sound](https://kenney.nl/assets/interface-sounds)
- [FreeSound.com](https://www.freesound.com) 



### Ambientazione

- [Free Low Poly Desert Pack](https://assetstore.unity.com/packages/3d/environments/free-low-poly-desert-pack-106709)
- [Low Poly Game Kit](https://assetstore.unity.com/packages/templates/packs/low-poly-game-kit-110455)
- [LowPoly Environment Pack](https://assetstore.unity.com/packages/3d/environments/landscapes/lowpoly-environment-pack-99479)
- [Low Poly Playground Pack](https://assetstore.unity.com/packages/3d/characters/low-poly-playground-pack-102503)
- [Low-Poly Simple Nature Pack](https://assetstore.unity.com/packages/3d/environments/landscapes/low-poly-simple-nature-pack-162153)
- [Low Poly Rock Pack](https://assetstore.unity.com/packages/3d/environments/low-poly-rock-pack-57874)
- [Low Poly Tree Pack](https://assetstore.unity.com/packages/3d/vegetation/trees/low-poly-tree-pack-57866)
- [LowPoly Water **Oceano animato**](https://assetstore.unity.com/packages/tools/particles-effects/lowpoly-water-107563)
- [POLYGON - Starter Pack](https://assetstore.unity.com/packages/3d/props/polygon-starter-pack-156819)
- [Simple Low Poly Nature Pack](https://assetstore.unity.com/packages/3d/environments/landscapes/simple-low-poly-nature-pack-157552)
- [Simplistic Low Poly Nature](https://assetstore.unity.com/packages/3d/environments/simplistic-low-poly-nature-93894)

### Tools

- [JSON .NET For Unity](https://assetstore.unity.com/packages/tools/input-management/json-net-for-unity-11347)



### Nemici

- [Free Low Polygon_Animal](https://assetstore.unity.com/packages/3d/characters/animals/free-low-polygon-animal-110679)

### Bauli

- [Low poly Chests](https://assetstore.unity.com/packages/3d/props/low-poly-chests-95342)