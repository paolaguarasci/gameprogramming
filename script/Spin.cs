using UnityEngine;

public class Spin : MonoBehaviour {
  public float speed = 3.0f;

  private void Update () {
    // Di default usa le coordinate locali (relative all'oggetto)
    // Per usare le coordinate globali (relative al mondo):
    // transform.Rotate(0, speed, 0, Space.World);

    transform.Rotate (0, speed, 0);

    // transform.Translate(0, speed, 0);
  }
}