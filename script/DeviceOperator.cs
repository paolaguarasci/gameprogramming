using UnityEngine;

public class DeviceOperator : MonoBehaviour {

  public float radius = 1.5f;

  // Start is called before the first frame update
  private void Start () {

  }

  // Update is called once per frame
  private void Update () {
    if (Input.GetKeyDown (KeyCode.E)) {
      Collider[] hitColliders = Physics.OverlapSphere (transform.position, radius);

      foreach (Collider hitCollider in hitColliders) {
        // controllo che l'oggetto sia davanti al player
        // usando il prodotto tra vettori 
        // che da 1 se e' esattamente nella stessa direzione
        // e -1 se e' esattamente nella direzione opposta
        Vector3 direction = hitCollider.transform.position - transform.position;
        if (Vector3.Dot (transform.forward, direction) > 0.5f) {
          hitCollider.SendMessage ("Operate", SendMessageOptions.DontRequireReceiver);
        }
      }
    }
  }
}