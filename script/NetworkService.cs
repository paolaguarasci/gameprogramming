using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Networking;

public class NetworkService {
  private const string key = "82e748baaff416ac5b9caa1caed46dc8";
  private const string urlX = "https://api.openweathermap.org/data/2.5/weather?q=Roma,it&mode=XML&appid=";
  private const string urlJ = "https://api.openweathermap.org/data/2.5/weather?q=Mumbai&appid=";

  private const string xmlAPI = urlX + key;
  private const string jsonAPI = urlJ + key;
  public IEnumerator GetWeatherXML (Action<string> callback) {
    Debug.Log ("Weather manager GetWeatherXML...");
    return CallAPI (xmlAPI, null, callback);
  }
  public IEnumerator GetWeatherJSON (Action<string> callback) {
    Debug.Log ("Weather manager GetWeatherJSON...");
    return CallAPI (jsonAPI, null, callback);
  }

  private IEnumerator CallAPI (string url, Hashtable args, Action<string> callback) {
    Debug.Log ("Weather manager CallAPI...");
    UnityWebRequest www;

    if (args == null) {
      www = UnityWebRequest.Get (url);
    } else {

      WWWForm form = new WWWForm ();
      foreach (DictionaryEntry arg in args) {
        form.AddField (arg.Key.ToString (), arg.Value.ToString ());
      }
      www = UnityWebRequest.Post (url, form);
    }

    yield return www.SendWebRequest ();
    if (IsResponseValid (www)) {
      callback (www.downloadHandler.text);
    }
  }

  private bool IsResponseValid (UnityWebRequest www) {
    Debug.Log ("Weather manager IsResponseValid...");
    if (www.error != null) {
      Debug.Log ("Errore di connessione");
      return false;
    } else if (string.IsNullOrEmpty (www.downloadHandler.text)) {
      Debug.Log ("Errore nei dati");
      return false;
    }
    return true;
  }

  private string webImg = "https://upload.wikimedia.org/wikipedia/commons/7/77/Unical_cubi.jpg";

  public IEnumerator DownloadImage (Action<Texture2D> callback) {
    Debug.Log ("DownloadImage");
    UnityWebRequest www = UnityWebRequestTexture.GetTexture (webImg);
    yield return www.SendWebRequest ();
    Texture2D dowloadedTexture = DownloadHandlerTexture.GetContent (www);
    callback (dowloadedTexture);
  }

  private string saveServer = "http://localhost:3000/save";

  public IEnumerator LogWeather (string name, float cloudValue, Action<string> callback) {
    Hashtable args = new Hashtable ();
    args.Add ("message", name);
    args.Add ("cloud_value", cloudValue);
    args.Add ("timestamp", DateTime.UtcNow);
    return CallAPI (saveServer, args, callback);
  }

}