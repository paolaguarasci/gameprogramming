using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;
using UnityEditor.VersionControl;
using UnityEngine;
using UnityEngine.UI;

public class SettingsPopup : MonoBehaviour {
  [SerializeField] private Text nameLabel;
  [SerializeField] private AudioClip sound;
  public void Open () {
    gameObject.SetActive (true);
    PauseGame ();
  }
  public void Close () {
    gameObject.SetActive (false);
    UnPauseGame ();
  }

  public void OnSubmitName (string name) {
    Debug.Log ("Name: " + name);
    nameLabel.text = name;
  }

  public void OnSpeedValue (float speed) {
    // Debug.Log("Speed: " + speed);
    Messenger<float>.Broadcast (GameEvent.SPEED_CHANGED, speed);
  }

  public void PauseGame () {
    GameEvent.isPaused = true;
    Cursor.lockState = CursorLockMode.None;
    Cursor.visible = true;
    Time.timeScale = 0f; // Ferma il gioco
  }

  public void UnPauseGame () {
    GameEvent.isPaused = false;
    Cursor.lockState = CursorLockMode.Locked;
    Cursor.visible = false;
    Time.timeScale = 1f; // Ferma il gioco
  }

  public void OnSoundToggle () {
    Managers.Audio.soundMute = !Managers.Audio.soundMute;
    Managers.Audio.PlaySound (sound);
  }
  public void OnSoundVolume (float value) {
    Managers.Audio.soundVolume = value;
  }

  public void OnPlayMusic (int selector) {
    Managers.Audio.PlaySound (sound);
    switch (selector) {
      case 1:
        Managers.Audio.PlayIntroMusic ();
        break;
      case 2:
        Managers.Audio.PlayLevelMusic ();
        break;
      case 3:
        Managers.Audio.StopMusic ();
        break;
    }
  }
}