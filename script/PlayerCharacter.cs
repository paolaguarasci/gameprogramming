using UnityEngine;
using UnityEngine.UI;

public class PlayerCharacter : MonoBehaviour {
  public int health;
  public int healtPackValue;
  private AudioSource _sourceSound;
  [SerializeField] private Slider healtBar;
  [SerializeField] private Image fillImage;
  [SerializeField] private Text gameOver;
  [SerializeField] private Image damageImage;
  [SerializeField] private AudioClip fxHurt;
  private Color flashColor = new Color (1f, 0f, 0f, 0.1f);
  private readonly float flashSpeed = 5f;
  private float barValueDamage;
  private Image healtBarBackground;
  private bool damaged;

  // Start is called before the first frame update
  private void Start () {
    health = Managers.Player.health;
    healtBar.maxValue = Managers.Player.maxHealth;
    healtPackValue = Managers.Player.healtPackValue;

    barValueDamage = healtBar.maxValue / health;

    healtBarBackground = healtBar.GetComponentInChildren<Image> ();
    gameOver.enabled = false;

    _sourceSound = GetComponent<AudioSource> ();
  }
  private void Update () {
    if (health < 0) {
      Death ();
    }

    if (Input.GetKeyDown (KeyCode.H) && Managers.Inventory.GetItemCount ("healt") != 0) {
      health += healtPackValue;
      healtBar.value += (barValueDamage * healtPackValue);

      if (health > Managers.Player.health) {
        health = Managers.Player.health;
        healtBar.value = healtBar.maxValue;
      }

      Managers.Inventory.ConsumeItem ("healt");

    }

    if (damaged) {
      _sourceSound.PlayOneShot (fxHurt);
    }

    damaged = false;
  }
  public void Hurt (int damage) {
    health -= damage;
    damaged = true;
    healtBar.value -= barValueDamage;
  }
  public void Death () {
    fillImage.enabled = false;
    gameOver.enabled = true;
    healtBarBackground.color = Color.red;

    GameEvent.isPaused = true;
    Cursor.lockState = CursorLockMode.None;
    Cursor.visible = true;
    Time.timeScale = 0f; // Ferma il gioco
  }

  public bool isLive () {
    return health > 0;
  }
}