using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorOpenAnimated : MonoBehaviour {
  [SerializeField] private Vector3 dPos;
  private AudioSource _sourceSound;
  private Vector3 _closePos;
  private Vector3 _openPos;

  private bool _open;
  private bool _doorIsMoving;

  // Start is called before the first frame update
  void Start () {
    _closePos = transform.position;
    _openPos = transform.position + dPos;
    _sourceSound = GetComponent<AudioSource> ();
  }

  // Update is called once per frame
  void Operate () {
    if (!_open) {
      if (!_doorIsMoving) {
        _doorIsMoving = true;
      } else {
        _open = true;
      }
    } else {
      if (!_doorIsMoving) {
        _doorIsMoving = true;
      } else {
        _open = false;
      }
    }
  }

  public void Activate () {
    _sourceSound.Play ();
    if (!_open) {
      _doorIsMoving = true;
    } else if (_doorIsMoving) {
      _open = false;
    }

  } // Operate

  public void Deactivate () {
    _sourceSound.Play ();
    if (_open) {
      _doorIsMoving = true;
    } else if (_doorIsMoving) {
      _open = true;
    }

  } // Operate

  private void Update () {
    if (_doorIsMoving) {
      doorAnimation ();
    }
  }

  void doorAnimation () {
    if (!_open) {
      if (transform.position != _openPos) {
        transform.position = Vector3.Lerp (transform.position, _openPos, 3f * Time.deltaTime);
      } else {
        _doorIsMoving = false;
        _open = true;
        _sourceSound.Stop ();
      }
    } else {
      if (transform.position != _closePos) {
        transform.position = Vector3.Lerp (transform.position, _closePos, 3f * Time.deltaTime);
      } else {
        _doorIsMoving = false;
        _open = false;
        _sourceSound.Stop ();
      }
    }
  }
}