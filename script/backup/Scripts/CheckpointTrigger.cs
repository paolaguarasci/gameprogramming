using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckpointTrigger : MonoBehaviour {
  public string identifier;
  private bool _triggered;
  [SerializeField] private AudioClip fx;
  private void OnTriggerEnter (Collider other) {
    if (other.GetComponent<CharacterController> ()) {
      if (_triggered) {
        return;
      } else {

        this.GetComponent<AudioSource> ().PlayOneShot (fx);
        _triggered = true;
      }
    }
  }
}