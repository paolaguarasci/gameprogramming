using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fireball : MonoBehaviour {
  public float speed = 5.0f;
  public int damage = 1;
  // Start is called before the first frame update
  void Start () {
    transform.position = new Vector3 (transform.position.x, 1.4f, transform.position.z);
  }

  // Update is called once per frame
  void Update () {
    transform.Translate (0, 0, speed * Time.deltaTime);
  }

  void OnTriggerEnter (Collider other) {
    PlayerCharacter player = other.GetComponent<PlayerCharacter> ();
    if (player != null) {
      Debug.Log ("Player HIT");
      player.Hurt (damage);
    }
    Destroy (this.gameObject);
  }
}