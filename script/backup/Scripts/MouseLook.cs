using UnityEngine;

public class MouseLook : MonoBehaviour {
  public enum RotationAxes {
    MouseXandY = 0,
    MouseX = 1,
    MouseY = 2
  }

  public RotationAxes axes = RotationAxes.MouseX;
  // velocita' di rotazione orizzontale
  public float sensitivityHor = 9.0f;
  // velocita' di rotazione verticale
  public float sensitivityVer = 9.0f;

  // minima rotazione verticale
  public float minVert = -45.0f;

  // massima rotazione verticale
  public float maxVert = +45.0f;

  // angolo verticale (che serve?)
  private float _rotationX = 0;

  private void Start () {
    // La rotazione del player deve essere legata solo 
    // al movimento del mouse e non alla fisica
    Rigidbody body = GetComponent<Rigidbody> ();
    if (body != null) {
      body.freezeRotation = true;
    }

  }

  private void Update () {

    if (!GameEvent.isPaused) {
      // Input.GetAxis("Mouse X") --> ottengo il movimento del mouse sull'asse orizzontale
      // Input.GetAxis("Mouse X") --> ottengo il movimento del mouse sull'asse verticale
      // Mathf.Clamp()            --> limita un angolo tra un minimo e un massimo di ampiezza

      if (axes == RotationAxes.MouseX) {
        transform.Rotate (0, Input.GetAxis ("Mouse X") * sensitivityHor, 0);
      } else if (axes == RotationAxes.MouseY) {
        _rotationX -= Input.GetAxis ("Mouse Y") * sensitivityVer;
        _rotationX = Mathf.Clamp (_rotationX, minVert, maxVert);

        // prendo la y attuale, evito rotazioni orizzontali
        float rotationY = transform.localEulerAngles.y;

        transform.localEulerAngles = new Vector3 (_rotationX, rotationY, 0);

      } else {
        _rotationX -= Input.GetAxis ("Mouse Y") * sensitivityVer;
        _rotationX = Mathf.Clamp (_rotationX, minVert, maxVert);

        float delta = Input.GetAxis ("Mouse X") * sensitivityHor;
        float rotationY = transform.localEulerAngles.y + delta;

        transform.localEulerAngles = new Vector3 (_rotationX, rotationY, 0);
      }
    }
  }
}