using System;
using System.Collections;
using System.Collections.Generic;
using System.Xml;
using MiniJSON;
using UnityEngine;
public class WeatherManager : MonoBehaviour, IGameManager {
  private NetworkService _network;
  public ManagerStatus status { get; private set; }
  public float cloudValue { get; private set; }
  public void Startup () {
    _network = new NetworkService ();
    // StartCoroutine(_network.GetWeatherXML(OnXMLDataLoaded));        
    StartCoroutine (_network.GetWeatherJSON (OnJSONDataLoaded));
    status = ManagerStatus.Initializating;
    Debug.Log ("Weather manager starting...");
  }
  public void LogWeather (string name) {
    // StartCoroutine(_network.LogWeather(name, cloudValue, OnLogged));
  }

  private void OnLogged (string res) {
    Debug.Log ("RESPONSE: " + res);
  }
  public void OnXMLDataLoaded (string data) {
    Debug.Log (data);

    XmlDocument doc = new XmlDocument ();
    doc.LoadXml (data);
    XmlNode root = doc.DocumentElement;
    XmlNode node = root.SelectSingleNode ("clouds");
    string value = node.Attributes["value"].Value;
    cloudValue = Convert.ToInt32 (value) / 100f;
    Debug.Log ("Value: " + cloudValue);
    // Messenger.Broadcast(GameEvent.WEATHER_UPDATE);

    status = ManagerStatus.Started;
  }
  public void OnJSONDataLoaded (string data) {
    Dictionary<string, object> dict;
    dict = (Dictionary<string, object>) Json.Deserialize (data);
    Dictionary<string, object> clouds = (Dictionary<string, object>) dict["clouds"];
    cloudValue = Convert.ToInt32 (clouds["all"]) / 100f;
    Debug.Log ("Value: " + cloudValue);
    // Messenger.Broadcast(GameEvent.WEATHER_UPDATE);

    status = ManagerStatus.Started;
  }
}