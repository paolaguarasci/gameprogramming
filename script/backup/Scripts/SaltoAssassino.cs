using UnityEngine;

public class SaltoAssassino : MonoBehaviour {
  void OnTriggerEnter (Collider other) {
    WanderingAI enemy = other.GetComponent<WanderingAI> ();
    if (enemy != null) {
      Vector3 direction = other.transform.position - transform.position;
      if (Vector3.Dot (transform.forward, direction) > 0.5f) {
        Debug.Log ("Enemy HIT");
        Destroy (other.gameObject);
      }
    }
  }
}