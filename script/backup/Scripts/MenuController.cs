using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MenuController : MonoBehaviour
{
    [SerializeField] public InputField nameField;
    [SerializeField] private Texture2D cursorArrow;
    private void Start()
    {
        string name = PlayerPrefs.GetString("nome");
        Cursor.SetCursor(cursorArrow, Vector2.zero, CursorMode.ForceSoftware);
        if (name != null)
        {
            nameField.text = name;
        }
    }

    public void OnLoadGame()
    {

        LoadingData.sceneToLoad = "Game";
        SceneManager.LoadScene("Loading");

    }

    public void OnSetName(string name)
    {
        PlayerPrefs.SetString("nome", name);
        PlayerPrefs.SetInt("score", 0);
        Debug.Log("Set nome " + name);
    }
}