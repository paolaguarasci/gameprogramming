using System.Collections;
using UnityEngine;

public class ReactiveTarget : MonoBehaviour {
  public void ReactToHit () {
    WanderingAI behavior = GetComponent<WanderingAI> ();
    if (behavior != null) {
      behavior.SetAlive (false);
    }
    StartCoroutine (Die ());
  }

  private IEnumerator Die () {
    this.transform.position = new Vector3 (this.transform.position.x, this.transform.position.y - 0.75f, this.transform.position.z);
    yield return new WaitForSeconds (1);

    Destroy (this.gameObject);
  }
}