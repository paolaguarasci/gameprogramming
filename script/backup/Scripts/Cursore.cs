using System.Collections;
using UnityEngine;

public class Cursore : MonoBehaviour {
  public Texture2D cursorTexture;
  public CursorMode cursorMode = CursorMode.Auto;
  public Vector2 hotSpot = Vector2.zero;
  void OnMouseEnter () {
    hotSpot = this.transform.position;
    Cursor.SetCursor (cursorTexture, hotSpot, cursorMode);
  }

  void OnMouseExit () {
    Cursor.SetCursor (null, Vector2.zero, cursorMode);
  }
}