using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UIController : MonoBehaviour {

  [SerializeField] private Text scoreLabel;
  [SerializeField] private Text lifeLabel;
  [SerializeField] private Text nameLabel;
  [SerializeField] private GameObject player;
  [SerializeField] private Texture2D cursorArrow;

  [SerializeField] private MenuPausa menuPausa;
  private PlayerCharacter pc;
  private bool pausa;

  void Awake () {
    Messenger.AddListener (GameEvent.COLLECT_COIN, OnCollectsCoin);
    Messenger.AddListener (GameEvent.LIFE_DOWN, OnLifeDown);
  }

  private void OnDestroy () {
    Messenger.AddListener (GameEvent.COLLECT_COIN, OnCollectsCoin);
    Messenger.AddListener (GameEvent.LIFE_DOWN, OnLifeDown);
  }

  // Start is called before the first frame update
  private void Start () {
    Cursor.SetCursor (cursorArrow, Vector2.zero, CursorMode.ForceSoftware);
    menuPausa.Close ();
    pausa = false;
    pc = player.GetComponent<PlayerCharacter> ();
    OnCollectsCoin ();
    OnLifeDown ();
    string name = PlayerPrefs.GetString ("nome");
    if (name != null) {
      nameLabel.text = name;

    } else {
      nameLabel.text = "";
    }

  }
  public void OnCollectsCoin () {
    scoreLabel.text = pc.score.ToString ();
    Debug.Log ("Score: " + pc.score);
  }

  public void OnLifeDown () {
    lifeLabel.text = pc.health.ToString ();
    Debug.Log ("Health: " + pc.health);
  }

  private void Update () {
    if (Input.GetKeyDown (KeyCode.Escape)) {
      if (pc.health <= 0) {
        // SceneManager.LoadScene ("GameOver");
                  SceneManager.LoadScene ("loadingGameOver");
      } else if (!pausa) {
        menuPausa.Open ();
        pausa = true;

      } else if (pausa) {
        menuPausa.Close ();
        pausa = false;

      }
    }

  }

  public void OnOpenSettings () {
    Debug.Log ("Open settings");
    // settingsPopup.Open ();
  }

}