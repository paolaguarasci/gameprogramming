using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent (typeof (PlayerManager))]
[RequireComponent (typeof (InventoryManager))]
[RequireComponent (typeof (WeatherManager))]
[RequireComponent (typeof (ImagesManager))]
[RequireComponent (typeof (AudioManager))]
[RequireComponent (typeof (LeaderboardManager))]
public class Managers : MonoBehaviour {

  public static PlayerManager Player { get; private set; }
  public static InventoryManager Inventory { get; private set; }
  public static ImagesManager Images { get; private set; }
  public static AudioManager Audio { get; private set; }
  public static LeaderboardManager board { get; private set; }
  public static LightManager lights { get; private set; }

  private List<IGameManager> _startSequence;

  private void Awake () {
    Player = GetComponent<PlayerManager> ();
    Inventory = GetComponent<InventoryManager> ();

    Images = GetComponent<ImagesManager> ();
    Audio = GetComponent<AudioManager> ();
    board = GetComponent<LeaderboardManager> ();
    lights = GetComponent<LightManager> ();

    _startSequence = new List<IGameManager> ();
    _startSequence.Add (Player);
    _startSequence.Add (Inventory);

    _startSequence.Add (Images);
    _startSequence.Add (Audio);
    _startSequence.Add (board);
    _startSequence.Add (lights);

    StartCoroutine (StartupManagers ());
  }

  private IEnumerator StartupManagers () {
    foreach (IGameManager manager in _startSequence) {
      if (manager != null) manager.Startup ();
    }

    yield return null;

    int numModules = _startSequence.Count;
    int numReady = 0;

    while (numReady < numModules) {
      int lastReady = numReady;
      numReady = 0;
      foreach (IGameManager manager in _startSequence) {
        if ((manager != null)  && manager.status == ManagerStatus.Started) {
          numReady++;
        }
      }

      if (numReady > lastReady) {
        Debug.Log ("Progress: " + numReady + "/" + numModules);
      }
      yield return null;
    }
    Debug.Log ("All managers started up!");
  }

}