using System.Collections;
using UnityEngine;
using UnityEngineInternal;

/*
Cosi con solo questa istruzione funziona, si muove 
transform.Translate(deltaX, 0, deltaZ);
ma va troppo veloce e h e v sono relativi al mondo
va reso Frame Indipendent moltiplicando per Time.deltaTime il movimento
cosi scala la velocita' in base al tempo tra un frame e l'altro

ClampMagnitude serve a limitare la velocita' di movimento diagonale
e ridurla a quella lungo le assi

*/

[RequireComponent (typeof (CharacterController))]
[AddComponentMenu ("Control Script/FPS Input")]

public class FPSInput : MonoBehaviour {
  public float speed = 6.0f;
  public float gravity = -9.8f;
  private readonly float baseSpeed = 6.0f;

  private AudioSource _soundSource;
  [SerializeField] private AudioClip footStepSound;
  private float _footStepSoundLenght;
  private bool _step;

  private CharacterController _characterController;
  private void Awake () {
    Messenger<float>.AddListener (GameEvent.SPEED_CHANGED, OnSpeedChanged);
  }

  private void OnDestroy () {
    Messenger<float>.RemoveListener (GameEvent.SPEED_CHANGED, OnSpeedChanged);
  }

  private void OnSpeedChanged (float value) {
    speed = baseSpeed * value;
    Debug.Log ("Player Speed: " + speed);
  }

  private void Start () {
    _characterController = GetComponent<CharacterController> ();
    _soundSource = GetComponent<AudioSource> ();
    _step = true;
    _footStepSoundLenght = 0.30f;
  }

  private void Update () {
    if (!GameEvent.isPaused) {
      float deltaX = Input.GetAxis ("Horizontal") * speed;
      float deltaZ = Input.GetAxis ("Vertical") * speed;

      Vector3 movement = new Vector3 (deltaX, 0, deltaZ);
      movement = Vector3.ClampMagnitude (movement, speed);
      movement *= Time.deltaTime;

      movement.y = gravity;

      movement = transform.TransformDirection (movement);

      _characterController.Move (movement);

      if (_characterController.velocity.magnitude > 1f && _step) {
        _soundSource.PlayOneShot (footStepSound);
        StartCoroutine (WaitForFootSteps (_footStepSoundLenght));

      }

    }
  }

  IEnumerator WaitForFootSteps (float stepsLenght) {
    _step = false;
    yield return new WaitForSeconds (stepsLenght);
    _step = true;
  }
}