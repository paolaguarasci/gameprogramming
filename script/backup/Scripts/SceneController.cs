using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneController : MonoBehaviour
{
    // mi prendo il prefab dall'inspector
    // Serialize Field e' un modo per dire che non e' 
    // proprio pubblica ma l'editor puo' accedervi
    [SerializeField] private GameObject chickenPrefab;
    [SerializeField] private GameObject condorPrefab;
    [SerializeField] private GameObject dragonPrefab;
    [SerializeField] private GameObject coinsPrefab;
    private GameObject[] _chicken;
    private GameObject[] _condor;
    private GameObject[] _dragon;
    private GameObject[] _coins;
    public int chickenCount;
    public int condorCount;
    public int dragonCount;
    public int coinsCount;
    public float speed = 3.0f;
    private float baseSpeed = 3.0f;
    private void Awake()
    {
        Messenger<float>.AddListener(GameEvent.SPEED_CHANGED, OnSpeedChanged);
    }

    private void OnDestroy()
    {
        Messenger<float>.RemoveListener(GameEvent.SPEED_CHANGED, OnSpeedChanged);
    }

    private void OnSpeedChanged(float value)
    {
        speed = baseSpeed * value;
    }

    private void Start()
    {
        Debug.Log("Scene Count " + "  " + SceneManager.GetActiveScene().name + "  " + SceneManager.sceneCount);
        _chicken = new GameObject[chickenCount];
        _condor = new GameObject[condorCount];
        _dragon = new GameObject[dragonCount];
        _coins = new GameObject[coinsCount];

        // for (int i = 0; i < coinsCount; i++) {
        //   _coins[i] = Instantiate (coinsPrefab);
        //   _coins[i].transform.position = new Vector3 (Random.Range (-23f, 23f), 1f, Random.Range (-23f, 23f));
        //   float angle = Random.Range (0, 360f);
        //   _coins[i].transform.Rotate (0, angle, 0);
        // }
    }

    private void Update()
    {
        for (int i = 0; i < _chicken.Length; i++)
        {
            if (_chicken[i] == null)
            {
                // Copio l'oggetto prefab
                // Instantiate ritorna un tipo generico
                // va fatto il typecast con "as"
                _chicken[i] = Instantiate(chickenPrefab);
                _chicken[i].GetComponent<GallinaAI>().speed = speed;
                _chicken[i].transform.position = new Vector3(Random.Range(50f, 125f), 1f, Random.Range(-24f, 24f));
                // _chicken[i].transform.position = new Vector3 (50f + ( 10f * i), 1f, -24f + (10f * i));
                float angle = Random.Range(0, 360f);
                _chicken[i].transform.Rotate(0, angle, 0);
            }
        }
        for (int i = 0; i < _condor.Length; i++)
        {
            if (_condor[i] == null)
            {
                // Copio l'oggetto prefab
                // Instantiate ritorna un tipo generico
                // va fatto il typecast con "as"
                _condor[i] = Instantiate(condorPrefab);
                _condor[i].GetComponent<GallinaAI>().speed = speed;
                _condor[i].transform.position = new Vector3(Random.Range(75f, 225f), 1f, Random.Range(25f, 75f));
                float angle = Random.Range(0, 360f);
                _condor[i].transform.Rotate(0, angle, 0);
            }
        }
        for (int i = 0; i < _dragon.Length; i++)
        {
            if (_dragon[i] == null)
            {
                // Copio l'oggetto prefab
                // Instantiate ritorna un tipo generico
                // va fatto il typecast con "as"
                _dragon[i] = Instantiate(dragonPrefab);
                _dragon[i].GetComponent<GallinaAI>().speed = speed;
                _dragon[i].transform.position = new Vector3(Random.Range(175f, 225f), 1f, Random.Range(125f, 175f));
                float angle = Random.Range(0, 360f);
                _dragon[i].transform.Rotate(0, angle, 0);
            }
        }

    }
}