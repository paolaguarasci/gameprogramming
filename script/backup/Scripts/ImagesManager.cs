using System;
using UnityEngine;

public class ImagesManager : MonoBehaviour, IGameManager {
  private NetworkService _network;
  public ManagerStatus status { get; private set; }
  private Texture2D _webImage;
  public void Startup () {
    Debug.Log ("Images manager starting...");
    _network = new NetworkService ();
    status = ManagerStatus.Started;
  }

  public void GetWebImage (Action<Texture2D> callback) {
    if (_webImage == null) {
      // StartCoroutine(_network.DownloadImage(callback));
      StartCoroutine (_network.DownloadImage ((Texture2D image) => {

        _webImage = image;
        callback (_webImage);

      }));

    } else {
      callback (_webImage);
    }
  }
}