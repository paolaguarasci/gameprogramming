using UnityEngine;

public class OnOffSwitchDevice : MonoBehaviour {
  private bool _on;
  [SerializeField] private Material offColor;
  [SerializeField] private Material onColor;
  [SerializeField] private GameObject target;
  public bool requireKey;
  private void Start () {
    _on = false;
  }

  private void Operate () {

    if (_on) {
      if (requireKey && Managers.Inventory.GetItemCount ("key") == 0) {
        return;
      }
      GetComponent<Renderer> ().material = onColor;
      target.SendMessage ("Activate");
      _on = false;

      if (requireKey) {
        Managers.Inventory.ConsumeItem ("key");
      }

    } else {
      GetComponent<Renderer> ().material = offColor;
      target.SendMessage ("Deactivate");
      _on = true;
    }
  }
}