using UnityEngine;

public class WanderingAI_fire : MonoBehaviour {
  private bool _alive = true;
  public float speed = 3.0f;
  private float baseSpeed = 3.0f;
  public float obstacleRange = 5.0f;

  [SerializeField] private GameObject fireballPrefab;
  private GameObject _fireball;

  private void Awake () {
    Messenger<float>.AddListener (GameEvent.SPEED_CHANGED, OnSpeedChanged);
  }

  private void OnDestroy () {
    Messenger<float>.RemoveListener (GameEvent.SPEED_CHANGED, OnSpeedChanged);
  }
  void OnSpeedChanged (float value) {
    speed = baseSpeed * value;
    Debug.Log ("Enemy Speed: " + speed);
  }
  private void Start () {
    _alive = true;
  }

  private void Update () {
    if (_alive) // si muove solo se vivo!!
    {
      // Va avanti sempre, ad ogni frame, senza tenere conto della rotazione
      transform.Translate (0, 0, speed * Time.deltaTime);

      // Un raggio che parte dalla posizione del personaggio
      // e va nella stessa direzione
      Ray ray = new Ray (transform.position, transform.forward);

      // usa un "raggio" a sezione circolare ampio quanto il personaggio
      if (Physics.SphereCast (ray, 0.75f, out RaycastHit hit)) {
        GameObject hitObject = hit.transform.gameObject;
        if (hitObject.GetComponent<PlayerCharacter> ()) {
          _fireball = Instantiate (fireballPrefab) as GameObject;
          _fireball.transform.position = transform.TransformPoint (Vector3.forward * 1.5f);
          _fireball.transform.rotation = transform.rotation;

        }
        // se l'oggetto colpito e' ad una distanza inferiore al limite
        // mi volto di un angolo random tra -110, 110 gradi.
        else if (hit.distance < obstacleRange) {
          float angle = Random.Range (-110, 110);
          transform.Rotate (0, angle, 0);
        }
      }
    }
  }

  public bool isAlive () { return _alive; }
  public void SetAlive (bool alive) {
    _alive = alive;
  }
}