using UnityEngine;

public class LightManager : MonoBehaviour, IGameManager {
    private NetworkService _network;
    public ManagerStatus status { get; private set; }

    [SerializeField] private Light sole;

    public float Luminosita {
        get { return sole.intensity; }
        set {
            if ((value < 0.5f)) {
                value = 0.5f;
            } else if (value >= 2.5f) {
                value = 2.5f;
            }

            sole.intensity = value;

        }
    }

    public void Startup () {
        Debug.Log ("Light manager starting...");
        sole.intensity = 1.5f;
        sole.shadowStrength = 0.3f;
        status = ManagerStatus.Started;

    }

}