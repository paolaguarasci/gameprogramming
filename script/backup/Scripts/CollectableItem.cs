using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SocialPlatforms.Impl;

public class CollectableItem : MonoBehaviour {
  [SerializeField] private string itemName;
  [SerializeField] private int score;
  [SerializeField] private AudioClip fx;

  private void OnTriggerEnter (Collider other) {
    if (other.GetComponent<CharacterController> ()) {
      GetComponent<AudioSource> ().PlayOneShot (fx);
      Debug.Log ("Item collected: " + itemName);
      Destroy (this.gameObject);
      Managers.Inventory.AddItem (itemName);
      other.GetComponent<PlayerCharacter> ().score += score;
      Messenger.Broadcast (GameEvent.COLLECT_COIN);

    }
  }
}