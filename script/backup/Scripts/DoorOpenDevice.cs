using UnityEngine;

public class DoorOpenDevice : MonoBehaviour {
  [SerializeField] private Vector3 dPos;
  private bool _open;

  private void Start () {

  }

  private void Update () {

  }

  public void Operate () {
    if (_open) {
      Vector3 pos = transform.position - dPos;
      transform.position = pos;
    } else {
      Vector3 pos = transform.position + dPos;
      transform.position = pos;
    }
    _open = !_open;
  } // Operate

  public void Activate () {
    if (!_open) {
      Vector3 pos = transform.position + dPos;
      transform.position = pos;
      _open = true;
    }

  } // Operate

  public void Deactivate () {
    if (_open) {
      Vector3 pos = transform.position - dPos;
      transform.position = pos;
      _open = false;
    }

  } // Operate
}