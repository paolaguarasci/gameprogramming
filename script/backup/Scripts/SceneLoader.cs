﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SceneLoader : MonoBehaviour
{

    [SerializeField] private Text loadingText;
    private bool loadScene;

    private void Start()
    {
        loadScene = false;
        Debug.Log("Scene Attuale " + SceneManager.GetActiveScene().name);
        Debug.Log("Scene Da Caricare " + LoadingData.sceneToLoad);
        Debug.Log("Starting " + Time.time + " seconds");
        StartCoroutine(LoadNewScene());
    }

    void Update()
    {
        if (loadScene == false)
        {
            loadScene = true;
            loadingText.text = "Loading...";
        }
        if (loadScene == true)
        {
            loadingText.color = new Color(loadingText.color.r, loadingText.color.g, loadingText.color.b, Mathf.PingPong(Time.time, 1));
        }
    }

    public IEnumerator LoadNewSceneAsync()
    {
        yield return new WaitForSeconds(3f);
        AsyncOperation async = SceneManager.LoadSceneAsync(LoadingData.sceneToLoad);
        while (!async.isDone)
        {
            yield return null;
        }
        Debug.Log("Coroutine ended: " + Time.time + " seconds");

    }
    public IEnumerator LoadNewScene()
    {
       yield return new WaitForSeconds(1f);
       SceneManager.LoadScene(LoadingData.sceneToLoad);
       Debug.Log("Coroutine ended: " + Time.time + " seconds");
    }

}