using System.Collections;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PlayerCharacter : MonoBehaviour {
  private bool save;
  public int health;
  public int healtPackValue;
  private AudioSource _sourceSound;
  [SerializeField] private Slider healtBar;
  [SerializeField] private Image fillImage;
  [SerializeField] private Text gameOver;
  [SerializeField] private Image damageImage;
  [SerializeField] private AudioClip fxHurt;
  private Color flashColor = new Color (1f, 0f, 0f, 0.1f);
  private readonly float flashSpeed = 5f;
  private float barValueDamage;
  private Image healtBarBackground;
  private bool damaged;
  private int _score;
  public int score { get { return _score; } set { _score = value; } }
  // Start is called before the first frame update
  private void Start () {
    save = false;
    health = Managers.Player.health;
    healtBar.maxValue = Managers.Player.maxHealth;
    healtPackValue = Managers.Player.healtPackValue;

    barValueDamage = healtBar.maxValue / health;

    healtBarBackground = healtBar.GetComponentInChildren<Image> ();
    gameOver.enabled = false;
    _score = 0;
    _sourceSound = GetComponent<AudioSource> ();
  }
  private void Update () {
    if (health < 0 || transform.position.y < -10) {
      Death ();
    }

    if (Input.GetKeyDown (KeyCode.H) && Managers.Inventory.GetItemCount ("healt") != 0) {
      health += healtPackValue;
      healtBar.value += (barValueDamage * healtPackValue);

      if (health > Managers.Player.health) {
        health = Managers.Player.health;
        healtBar.value = healtBar.maxValue;
      }

      Managers.Inventory.ConsumeItem ("healt");

    }

    if (damaged) {
      _sourceSound.PlayOneShot (fxHurt);
    }

    damaged = false;
  }
  public void Hurt (int damage) {
    health -= damage;
    damaged = true;
    healtBar.value -= barValueDamage;
    Messenger.Broadcast (GameEvent.LIFE_DOWN);
  }
  public void Death () {
    fillImage.enabled = false;
    gameOver.enabled = true;
    healtBarBackground.color = Color.red;

    StartCoroutine (OnGameOver ());
    if (!save) {
      Managers.board.LogScore (PlayerPrefs.GetString ("nome"), score);
      save = true;
    }
    PlayerPrefs.SetInt ("score", score);
    GameEvent.isPaused = true;
    Cursor.lockState = CursorLockMode.None;
    Cursor.visible = true;
    Time.timeScale = 0f; // Ferma il gioco
  }

  public IEnumerator OnGameOver () {
    yield return new WaitForSeconds (2);
    // SceneManager.LoadScene ("GameOver");
              SceneManager.LoadScene ("loadingGameOver");
  }

  public bool isLive () {
    return health > 0;
  }
}