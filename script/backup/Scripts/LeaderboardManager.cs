using System.Collections.Generic;
using Newtonsoft.Json;
using UnityEngine;
using UnityEngine.UI;

public class Score {
  public string name;
  public string timestamp;
  public int score;
}

public class LeaderboardManager : MonoBehaviour, IGameManager {
  private NetworkService _network;
  public ManagerStatus status { get; private set; }
  public List<Score> scores;
  [SerializeField] private GameObject panel;
  public void Startup () {
    _network = new NetworkService ();

    StartCoroutine (_network.GetLeaderboard (OnJSONDataLoaded));
    status = ManagerStatus.Initializating;
    Debug.Log ("Weather manager starting...");
  }
  public void LogScore (string name, int score) {
    StartCoroutine (_network.SavePoint (name, score, OnLogged));
  }

  private void OnLogged (string res) {
    Debug.Log ("RESPONSE: " + res);
  }
  public void OnJSONDataLoaded (string data) {
    if (panel != null) {
      List<Score> scores = JsonConvert.DeserializeObject<List<Score>> (data);
      int k = 1;
      foreach (Score s in scores) {
        GameObject tmp = new GameObject ();
        Shadow shadow = tmp.AddComponent<Shadow> ();
        RectTransform rc = tmp.AddComponent<RectTransform> ();
        Text txt = tmp.AddComponent<Text> ();

        tmp.name = "Leader1";
        tmp.transform.SetParent (panel.transform);

        rc.sizeDelta = new Vector2 (100, 200);
        rc.localPosition = new Vector3 (0, 0, 0);

        txt.fontSize = 16;
        txt.font = Resources.GetBuiltinResource (typeof (Font), "Arial.ttf") as Font;

        txt.text = k.ToString () + ". ";
        txt.text += s.name;
        txt.text += " (" + s.score + " pts)";
        k++;
      }
    }
    status = ManagerStatus.Started;
  }
}