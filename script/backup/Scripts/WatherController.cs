using UnityEngine;

public class WatherController : MonoBehaviour {

  [SerializeField] private Material sky;
  [SerializeField] private Light sun;
  private float _fullIntensity;
  private float _cloudValue = 0f;

  void Awake () {
    // Messenger.AddListener(GameEvent.WEATHER_UPDATE, OnWeatherUpdate);
  }

  void OnDestroy () {
    // Messenger.RemoveListener(GameEvent.WEATHER_UPDATE, OnWeatherUpdate);
  }

  void OnWeatherUpdate () {
    // SetOvercast(Managers.Wheather.cloudValue);
  }

  void Start () {
    _fullIntensity = sun.intensity;
  }

  void SetOvercast (float value) {
    sky.SetFloat ("_Blend", value);
    sun.intensity = _fullIntensity - (_fullIntensity * value);

  }
}