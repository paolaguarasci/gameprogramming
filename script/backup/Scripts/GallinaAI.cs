using UnityEngine;

public class GallinaAI : MonoBehaviour {
  private bool _alive = true;
  public float speed = 3.0f;
  private float baseSpeed = 3.0f;
  public float obstacleRange = 5.0f;
  private bool insegui = false;
  [SerializeField] private GameObject fireballPrefab;
  private GameObject _fireball;
  private Vector3 posIniziale;
  private float distanzaDiFuga;
  [SerializeField] private float distanza;
  [SerializeField] private GameObject player;
  private PlayerCharacter pc;
  // Start is called before the first frame update
  void Start () {
    posIniziale = transform.position;
    distanza = 3f;
    _alive = true;
    insegui = false;
    distanzaDiFuga = 5f;
    pc = null;
  }

  private void Awake () {
    Messenger<float>.AddListener (GameEvent.SPEED_CHANGED, OnSpeedChanged);
  }

  private void OnDestroy () {
    Messenger<float>.RemoveListener (GameEvent.SPEED_CHANGED, OnSpeedChanged);
  }
  void OnSpeedChanged (float value) {
    speed = baseSpeed * value;
    Debug.Log ("Enemy Speed: " + speed);
  }

  private void Update () {
    if (_alive) // si muove solo se vivo!!
    {
      Ray ray = new Ray (transform.position, transform.forward);

      if (Physics.SphereCast (ray, 0.75f, out RaycastHit hit)) {
        GameObject hitObject = hit.transform.gameObject;
        if (hitObject.GetComponent<PlayerCharacter> () && hit.distance < distanzaDiFuga) {
          insegui = true;
          pc = hitObject.GetComponent<PlayerCharacter> ();
        } else if (insegui && hitObject.GetComponent<PlayerCharacter> () && hit.distance > distanzaDiFuga) {
          insegui = false;
          posIniziale = transform.position;
          pc = null;
        }
      }

      if (insegui && pc != null) {
        Debug.Log ("Posizione PLAYER (" + pc.transform.position.x + ", " + pc.transform.position.y + ", " + pc.transform.position.z + ")");

        transform.position = Vector3.MoveTowards (transform.position, pc.transform.position, speed * Time.deltaTime);
        transform.LookAt (pc.transform.position, Vector3.up);

        _fireball = Instantiate (fireballPrefab) as GameObject;
        _fireball.transform.position = transform.TransformPoint (Vector3.forward * 1.5f);
        _fireball.transform.rotation = transform.rotation;
      } else {
        transform.Translate (0, 0, speed * Time.deltaTime);
        float dist = Vector3.Distance (posIniziale, transform.position);
        if (dist > distanza) {
          float angle = 180;
          transform.Rotate (0, angle, 0);
        }

      }

    }
  }

  public bool isAlive () { return _alive; }
  public void SetAlive (bool alive) {
    _alive = alive;
  }
}