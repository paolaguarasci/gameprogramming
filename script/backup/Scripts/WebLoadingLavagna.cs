using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WebLoadingLavagna : MonoBehaviour {
  public void Operate () {
    Managers.Images.GetWebImage (OnWebImage);

  }

  private void OnWebImage (Texture2D obj) {
    Debug.Log ("OnWebImage");
    GetComponent<Renderer> ().material.mainTexture = obj;
  }
}