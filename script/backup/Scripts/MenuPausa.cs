using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuPausa : MonoBehaviour
{
    // [SerializeField] private Text nameLabel;
    [SerializeField] private AudioClip sound;

    public void Open()
    {
        gameObject.SetActive(true);
        PauseGame();
    }
    public void Close()
    {
        gameObject.SetActive(false);
        UnPauseGame();
    }

    public void OnSubmitName(string name)
    {
        Debug.Log("Name: " + name);
        // nameLabel.text = name;
    }

    public void OnSpeedValue(float speed)
    {
        // Debug.Log("Speed: " + speed);
        Messenger<float>.Broadcast(GameEvent.SPEED_CHANGED, speed);
    }

    public void PauseGame()
    {
        GameEvent.isPaused = true;
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
        Time.timeScale = 0f; // Ferma il gioco
    }

    public void UnPauseGame()
    {
        GameEvent.isPaused = false;
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
        Time.timeScale = 1f; // Ferma il gioco
    }

    public void OnSoundToggle()
    {
        Managers.Audio.soundMute = !Managers.Audio.soundMute;
        // Managers.Audio.PlaySound(sound);
    }
    public void OnSoundVolume(float value)
    {
        Managers.Audio.soundVolume = value;
    }

    public void OnPlayMusic(int selector)
    {
        // Managers.Audio.PlaySound(sound);
        switch (selector)
        {
            case 1:
                Managers.Audio.PlayIntroMusic();
                break;
            case 2:
                Managers.Audio.PlayLevelMusic();
                break;
            case 3:
                Managers.Audio.StopMusic();
                break;
        }
    }

    public void OnExit()
    {
        Debug.Log("Exit");
        LoadingData.sceneToLoad = "Menu";
        SceneManager.LoadScene("Loading");
    }
    public void OnVolumePiu()
    {
        Managers.Audio.PlaySound(sound);
        Managers.Audio.soundVolume += 0.1f;
        Debug.Log("Volume " + Managers.Audio.soundVolume);
    }
    public void OnVolumeMeno()
    {
        Managers.Audio.soundVolume -= 0.1f;
        Debug.Log("Volume " + Managers.Audio.soundVolume);
        Managers.Audio.PlaySound(sound);
    }
    public void OnLuminositaPiu()
    {
        Debug.Log("OnLuminositaPiu");
        Managers.Audio.PlaySound(sound);
    }
    public void OnLuminositaMeno()
    {
        Debug.Log("OnLuminositaMeno");
        Managers.Audio.PlaySound(sound);
    }

}