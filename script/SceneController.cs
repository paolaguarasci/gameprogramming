using UnityEngine;

public class SceneController : MonoBehaviour {
  // mi prendo il prefab dall'inspector
  // Serialize Field e' un modo per dire che non e' 
  // proprio pubblica ma l'editor puo' accedervi
  [SerializeField] private GameObject enemyPrefab;
  private GameObject[] _enemies;
  public int enemiesCount;
  public float speed = 3.0f;
  private float baseSpeed = 3.0f;
  private void Awake () {
    Messenger<float>.AddListener (GameEvent.SPEED_CHANGED, OnSpeedChanged);
  }

  private void OnDestroy () {
    Messenger<float>.RemoveListener (GameEvent.SPEED_CHANGED, OnSpeedChanged);
  }

  private void OnSpeedChanged (float value) {
    speed = baseSpeed * value;
  }

  private void Start () {
    _enemies = new GameObject[enemiesCount];
  }

  private void Update () {
    for (int i = 0; i < _enemies.Length; i++) {
      if (_enemies[i] == null) {
        // Copio l'oggetto prefab
        // Instantiate ritorna un tipo generico
        // va fatto il typecast con "as"
        _enemies[i] = Instantiate (enemyPrefab);
        _enemies[i].GetComponent<WanderingAI> ().speed = speed;
        _enemies[i].transform.position = new Vector3 (Random.Range (1f, 5f), 1f, Random.Range (1f, 5f));
        float angle = Random.Range (0, 360f);
        _enemies[i].transform.Rotate (0, angle, 0);
      }
    }
  }
}