using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent (typeof (CharacterController))]
public class RelativeMovement : MonoBehaviour {
  [SerializeField] private Transform target; //la camera
  public float rotationSpeed = 15.0f; //for smoothly rotation
  public float moveSpeed;
  public float walkSpeed = 6.0f;
  public float runSpeed = 10.0f;
  private CharacterController characterController;
  private ControllerColliderHit controllerColliderhit;

  //jump developing
  public float jumpSpeed = 15.0f;
  public float gravity = -9.8f;
  public float terminalVelocity = -10.0f;
  public float minFall = -1.5f;
  public float verticalSpeed;

  public float pushForce = 3.0f;

  // Animator
  private Animator _animator;

  // Start is called before the first frame update
  void Start () {
    verticalSpeed = minFall;
    moveSpeed = walkSpeed;
    characterController = GetComponent<CharacterController> ();
    _animator = GetComponent<Animator> ();
  }

  // Update is called once per frame
  void Update () {

    if (Input.GetKey ("left shift")) {
      moveSpeed = runSpeed;
      Debug.Log ("Corro!");
    } else {
      moveSpeed = walkSpeed;
      Debug.Log ("Cammino!");
    }

    Vector3 movement = Vector3.zero;
    float horizontal = Input.GetAxis ("Horizontal");
    float vertical = Input.GetAxis ("Vertical");

    if (horizontal != 0 || vertical != 0) {

      movement.x = horizontal * moveSpeed;
      movement.z = vertical * moveSpeed;
      movement = Vector3.ClampMagnitude (movement, moveSpeed);

      Quaternion tmp = target.rotation;
      target.eulerAngles = new Vector3 (0, target.eulerAngles.y, 0);
      movement = target.TransformDirection (movement);
      target.rotation = tmp;
      Quaternion direction = Quaternion.LookRotation (movement);
      transform.rotation = Quaternion.Lerp (transform.rotation, direction, rotationSpeed * Time.deltaTime);
    }

    bool hitGround = false;
    RaycastHit hit;
    if (verticalSpeed < 0 && Physics.Raycast (transform.position, Vector3.down, out hit)) {
      float check = (characterController.height + characterController.radius) / 1.9f; //1.9 perchè controllo un pò sotto i piedi e non esattamente sotto
      hitGround = hit.distance <= check;
    }
    _animator.SetFloat ("speed", movement.magnitude);
    if (hitGround) {
      if (Input.GetButtonDown ("Jump")) {
        verticalSpeed = jumpSpeed;
      } else {
        verticalSpeed = minFall;
        _animator.SetBool ("jumping", false);
      }
    } else {
      verticalSpeed += gravity * 5 * Time.deltaTime;
      if (verticalSpeed < terminalVelocity) verticalSpeed = terminalVelocity;

      _animator.SetBool ("jumping", true);
      if (characterController.isGrounded) {
        if (Vector3.Dot (movement, controllerColliderhit.normal) < 0) {
          movement = controllerColliderhit.normal * moveSpeed;
        } else {
          movement += controllerColliderhit.normal * moveSpeed;
        }
      }
    }

    movement.y = verticalSpeed;
    movement *= Time.deltaTime;
    characterController.Move (movement);
    Debug.Log ("Speed: " + moveSpeed);
  }

  private void OnControllerColliderHit (ControllerColliderHit hit) {
    controllerColliderhit = hit;

    Rigidbody body = hit.collider.attachedRigidbody;
    if (body != null && !body.isKinematic) {
      body.velocity = hit.moveDirection * pushForce;
    }
  }
}