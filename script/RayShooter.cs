using System.Collections;
using UnityEngine;
using UnityEngine.EventSystems;

public class RayShooter : MonoBehaviour {

  private Camera _camera;
  private AudioSource _soundSource;
  [SerializeField] private AudioClip hitEnemyFX;

  private void Start () {
    // Accesso ad un altro componente dello stesso oggetto
    _camera = GetComponent<Camera> ();

    // Blocca il cursore al centro dello schermo
    // Cursor.lockState = CursorLockMode.Locked;
    // Cursor.visible = false;

  }

  private void Update () {
    // Risponde alla pressione del pulsante 0 del mouse
    // per non sparare quando seleziono elementi dell'interfaccia
    if (Input.GetMouseButtonDown (0) && !EventSystem.current.IsPointerOverGameObject ()) {
      // Il centro dello schermo e'a meta'della sua altezza e dall sua larghezza
      Vector3 point = new Vector3 (_camera.pixelWidth / 2, _camera.pixelHeight / 2, 0);

      // crea il raggio da quella posizione
      Ray ray = _camera.ScreenPointToRay (point);

      // out ---> passaggio per riferimento
      if (Physics.Raycast (ray, out RaycastHit hit)) {

        // Ottengo l'oggetto che il raggio ha intersecato
        GameObject hitObject = hit.transform.gameObject;

        ReactiveTarget target = hitObject.GetComponent<ReactiveTarget> ();

        // controllo se l'oggetto intersecato ha lo script "Reactive Target" 
        if (target != null) {

          WanderingAI enemy = hitObject.GetComponent<WanderingAI> ();
          if (enemy.isAlive ()) {
            //Debug.Log("Target Hit " + hit.point);
            Messenger.Broadcast (GameEvent.ENEMY_HIT);
            _soundSource = enemy.GetComponent<AudioSource> ();
            _soundSource.PlayOneShot (hitEnemyFX);

          }

          target.ReactToHit ();
        } else {
          StartCoroutine (SphereIndicator (hit.point));
        }

      }
    }

  }

  private IEnumerator SphereIndicator (Vector3 point) {

    GameObject sphere = GameObject.CreatePrimitive (PrimitiveType.Sphere);
    sphere.transform.position = point;

    // Yield indica alla coroutine dove fermarsi
    yield return new WaitForSeconds (1);

    Destroy (sphere);
  }
  /*
      private void OnGUI()
      {
          int size = 12;
          float posX = _camera.pixelWidth / 2 - size / 4;
          float posY = _camera.pixelHeight / 2 - size / 2;

          GUI.Label(new Rect(posX, posY, size, size), "*");
      }

      */
}