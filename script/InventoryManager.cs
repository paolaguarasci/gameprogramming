using System.Collections.Generic;
using UnityEngine;

public class InventoryManager : MonoBehaviour, IGameManager {
  public ManagerStatus status { get; private set; } // non ho idea di cosa voglia dire...
  private Dictionary<string, int> _items;
  public void Startup () {
    Debug.Log ("Inventory manager starting...");
    status = ManagerStatus.Started;
    _items = new Dictionary<string, int> ();
  }

  public void DisplayItems () {
    string itemDisplay = "List of Items: ";
    foreach (KeyValuePair<string, int> item in _items) {
      itemDisplay += item.Key + " (" + item.Value + ") ";
    }

    Debug.Log (itemDisplay);
  }

  public void AddItem (string name) {
    if (_items.ContainsKey (name)) {
      _items[name] += 1;

    } else {
      _items[name] = 1;
    }
    DisplayItems ();
  }

  public List<string> GetItemsList () {
    List<string> list = new List<string> (_items.Keys);
    return list;
  }
  public int GetItemCount (string name) {
    if (_items.ContainsKey (name)) {
      return _items.Count;
    }
    return 0;
  }

  public void ConsumeItem (string name) {
    if (_items.ContainsKey (name)) {
      _items[name]--;
      if (_items[name] == 0) {
        _items.Remove (name);
      }
    } else {
      Debug.Log ("Non posso usare " + name);
    }
    DisplayItems ();
  }
}