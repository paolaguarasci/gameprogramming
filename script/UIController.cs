using UnityEngine;
using UnityEngine.UI;

public class UIController : MonoBehaviour {

  [SerializeField] private Text scoreLabel;
  [SerializeField] private GameObject player;
  [SerializeField] private SettingsPopup settingsPopup;
  private PlayerCharacter pc;
  private int _score;

  // 
  void Awake () {
    Messenger.AddListener (GameEvent.ENEMY_HIT, OnEnemyHit);
  }

  private void OnDestroy () {
    Messenger.RemoveListener (GameEvent.ENEMY_HIT, OnEnemyHit);
  }

  // Start is called before the first frame update
  private void Start () {
    _score = 0;
    scoreLabel.text = _score.ToString ();
    // pc = player.GetComponent<PlayerCharacter>();
    settingsPopup.Close ();
  }
  public void OnEnemyHit () {
    _score += 1;
    scoreLabel.text = _score.ToString ();
  }

  private void Update () {
    if (Input.GetKeyDown (KeyCode.Escape)) {
      settingsPopup.Open ();
    }
  }

  public void OnOpenSettings () {
    Debug.Log ("Open settings");
    settingsPopup.Open ();
  }

}