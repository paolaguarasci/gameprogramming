using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerManager : MonoBehaviour, IGameManager {
  public ManagerStatus status { get; private set; } // non ho idea di cosa voglia dire...

  public int health { get; private set; }
  public int maxHealth { get; private set; }
  public int healtPackValue { get; private set; }
  public int barValueDamage { get; private set; }

  public void Startup () {
    Debug.Log ("Player manager starting...");
    status = ManagerStatus.Started;
    health = 5;
    maxHealth = 100;
    healtPackValue = 2;
    barValueDamage = maxHealth / health;
  }

}