using UnityEngine;

public class AudioManager : MonoBehaviour, IGameManager {
  private NetworkService _network;
  public ManagerStatus status { get; private set; }

  [SerializeField] private AudioSource soundSource;
  [SerializeField] private AudioSource musicSource;
  [SerializeField] private string introBGMusic;
  [SerializeField] private string levelBGMusic;

  public float soundVolume {
    get { return AudioListener.volume; }
    set { AudioListener.volume = value; }
  }

  public bool soundMute {
    get { return AudioListener.pause; }
    set { AudioListener.pause = value; }
  }

  public void Startup () {
    Debug.Log ("Audio manager starting...");
    soundVolume = 1f;
    status = ManagerStatus.Started;
    musicSource.clip = (AudioClip) Resources.Load ("Music/" + introBGMusic);
    musicSource.Play ();
  }

  public void PlaySound (AudioClip clip) {
    soundSource.PlayOneShot (clip);
  }

  public void PlayIntroMusic () {

    PlayMusic ((AudioClip) Resources.Load ("Music/" + introBGMusic));

  }

  public void PlayLevelMusic () {

    PlayMusic ((AudioClip) Resources.Load ("Music/" + levelBGMusic));

  }

  public void PlayMusic (AudioClip clip) {

    musicSource.clip = clip;
    musicSource.Play ();
  }

  public void StopMusic () {
    musicSource.Stop ();
  }
}