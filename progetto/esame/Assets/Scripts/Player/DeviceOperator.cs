using UnityEngine;

public class DeviceOperator : MonoBehaviour {
    #region var
    public float radius = 1.5f;
    #endregion

    private void Update() {
        if (Input.GetKeyDown(KeyCode.E)) {
            Collider[] hitColliders = Physics.OverlapSphere(transform.position, radius);

            foreach (Collider hitCollider in hitColliders) {
                // controllo che l'oggetto sia davanti al player
                // usando il prodotto tra vettori 
                // che da 1 se e' esattamente nella stessa direzione
                // e -1 se e' esattamente nella direzione opposta
                Vector3 direction = hitCollider.transform.position - transform.position;
                if (Vector3.Dot(transform.forward, direction) > 0.5f) {
                    hitCollider.SendMessage("Operate", SendMessageOptions.DontRequireReceiver);
                }
            }
        }
    }
}