using UnityEngine;

public class CheckpointTrigger : MonoBehaviour {
    #region var
    public string identifier;
    private bool _triggered;
    [SerializeField] private AudioClip fx;
    #endregion 

    private void OnTriggerEnter(Collider other) {
        if (other.GetComponent<CharacterController>()) {
            if (_triggered) {
                return;
            } else {

                GetComponent<AudioSource>().PlayOneShot(fx);
                _triggered = true;
            }
        }
    }
}