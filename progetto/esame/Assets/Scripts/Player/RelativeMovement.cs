using UnityEngine;

public class RelativeMovement : MonoBehaviour {
    [SerializeField] private float _moveSpeed = 15f;
    [SerializeField] private float _walkSpeed = 15f;
    [SerializeField] private float _runSpeed = 35f;
    [SerializeField] private float _jumpSpeed = 0.5f;
    [SerializeField] private float _rotationSpeed = 5f;

    private float _gravity = 9.81f;
    private float _dirY;
    private bool _doppioSalto = false;
    private float _doppioSaltoMult = 0.3f;
    private CharacterController _characterController;
    private Transform _camera;

    private bool isJumping;


    private void Start() {
        _characterController = GetComponent<CharacterController>();
        _camera = FindObjectOfType<Camera>().transform;
        isJumping = false;
    }

    private void Update() {


        if (Input.GetKeyDown("left shift")) {
            Debug.Log("Corro!!");
            _moveSpeed = _runSpeed;
        } else {

            _moveSpeed = _walkSpeed;
        }


        float horizontalInput = Input.GetAxis("Horizontal");
        float verticalInput = Input.GetAxis("Vertical");
        Vector3 direction = new Vector3(horizontalInput, 0, verticalInput);
        direction = Vector3.ClampMagnitude(direction, _moveSpeed);

        if (_characterController.isGrounded) {
            _doppioSalto = true;
            if (Input.GetButtonDown("Jump")) {
                _dirY = _jumpSpeed;
            }
        } else {
            if (Input.GetButtonDown("Jump")) {
                _dirY = _jumpSpeed * _doppioSaltoMult;
                _doppioSalto = false;
            }
            _dirY -= _gravity * Time.deltaTime;
        }


        #region Camera
        Quaternion tmp = _camera.rotation;
        _camera.eulerAngles = new Vector3(0, _camera.eulerAngles.y, 0);
        direction = _camera.TransformDirection(direction);
        _camera.rotation = tmp;
        Quaternion newDirection = Quaternion.LookRotation(direction);
        transform.rotation = Quaternion.Lerp(transform.rotation, newDirection, _rotationSpeed * Time.deltaTime);
        #endregion Camera

        direction.y = _dirY;
        _characterController.Move(direction * _moveSpeed * Time.deltaTime);
    }


    public void OnCollisionEnter(Collision collision) {
        Debug.Log("Enemy COLLISION");
        if (collision.gameObject.GetComponent<ReactiveTarget>()) {
            Vector3 normalHitPoint = collision.contacts[0].normal;
            if (normalHitPoint.y > 0.5f) {
                Debug.Log("Enemy `SOTTO");
                ReactiveTarget enemy = collision.gameObject.GetComponent<ReactiveTarget>();
                enemy.reactToHit();
            }
        }
    }

}