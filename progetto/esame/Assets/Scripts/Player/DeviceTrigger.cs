using UnityEngine;

public class DeviceTrigger : MonoBehaviour {
    #region var
    [SerializeField] private GameObject[] targets = null;
    public bool requireKey;
    #endregion

    private void OnTriggerEnter(Collider other) {

        if (requireKey && InventoryManager.Instance.GetItemCount ("key") == 0) {
          return;
        }

        foreach (GameObject target in targets) {
            target.SendMessage("Activate");
        }
    }

    private void OnTriggerExit(Collider other) {
        foreach (GameObject target in targets) {
            target.SendMessage("Deactivate");
        }
    }
}