using System.Collections;
using UnityEngine;

public class PlayerCharacter : MonoBehaviour {
    #region var
    public int health;
    private AudioSource _sourceSound;
    [SerializeField] private AudioClip fxHurt;

    private bool damaged;
    #endregion

    private void Start() {
        _sourceSound = GetComponent<AudioSource>();
    }
    private void Update() {
        if (transform.position.y < -10) {
            // In caso di caduta
            PlayerManager.Instance.LifeDown(5);
        }

        if (Input.GetKeyDown(KeyCode.H) && InventoryManager.Instance.GetItemCount("healt") != 0) {
            health += PlayerManager.Instance.healtPackValue;

            if (health > PlayerManager.Instance.life) {
                health = PlayerManager.Instance.life;
            }

            InventoryManager.Instance.ConsumeItem("healt");

        }

        if (damaged) {
            _sourceSound.PlayOneShot(fxHurt);
        }

    }



    public void Hurt(int damage) {

        damaged = true;
        StartCoroutine(HurtCoo(damage));
        damaged = false;
    
    }

    public IEnumerator Blink() {
        MeshRenderer render = GetComponent<MeshRenderer>();
        render.enabled = false;
        yield return new WaitForSeconds(0.1f);
        render.enabled = true;
        yield return new WaitForSeconds(0.1f);
    }

    public IEnumerator HurtCoo(int damage) {
        if(damaged) { 
            PlayerManager.Instance.LifeDown(damage);
            // StartCoroutine(Blink());
            yield return new WaitForSeconds(5);
            damaged = false;
        } else {
            yield return null;
        }
    }
}