using UnityEngine;

public class OrbitCamera : MonoBehaviour {
    #region Variable Declaration
    [SerializeField] private Transform target;
    public float rotSpeed = 1.5f;
    private float _rotY;
    private Vector3 _offset;
    #endregion 

    private void Start() {
        _rotY = transform.eulerAngles.y - 90;
        _offset = target.position - transform.position;
    }

    // Altro metodo di MonoBehaviour, chiamato dopo gli update
    private void LateUpdate() {
        float horInput = Input.GetAxis("Horizontal");

        if (horInput != 0) {
            _rotY += horInput * rotSpeed;
        } else {
            //_rotY += Input.GetAxis("Mouse X") * rotSpeed * 3;
        }

        Quaternion rotation = Quaternion.Euler(0, _rotY, 0);
        transform.position = target.position - (rotation * _offset);
        transform.LookAt(target);
    }
}