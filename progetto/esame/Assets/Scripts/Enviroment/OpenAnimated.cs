using UnityEngine;

public class OpenAnimated : MonoBehaviour {
    #region var
    [SerializeField] private Vector3 dPos = Vector3.zero;
    private AudioSource _sourceSound;
    private Vector3 _closePos;
    private Vector3 _openPos;
    private bool _open;
    private bool _isMoving;
    #endregion 

    // Start is called before the first frame update
    private void Start() {
        _closePos = transform.position;
        _openPos = transform.position + dPos;
        _sourceSound = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    private void Operate() {
        if (!_open) {
            if (!_isMoving) {
                _isMoving = true;
            } else {
                _open = true;
            }
        } else {
            if (!_isMoving) {
                _isMoving = true;
            } else {
                _open = false;
            }
        }
    }

    public void Activate() {
        _sourceSound.Play();
        if (!_open) {
            _isMoving = true;
        } else if (_isMoving) {
            _open = false;
        }

    } // Operate

    public void Deactivate() {
        _sourceSound.Play();
        if (_open) {
            _isMoving = true;
        } else if (_isMoving) {
            _open = true;
        }

    } // Operate

    private void Update() {
        if (_isMoving) {
            openAnimation();
        }
    }

    private void openAnimation() {
        if (!_open) {
            if (transform.position != _openPos) {
                transform.position = Vector3.Lerp(transform.position, _openPos, 3f * Time.deltaTime);
            } else {
                _isMoving = false;
                _open = true;
                _sourceSound.Stop();
            }
        } else {
            if (transform.position != _closePos) {
                transform.position = Vector3.Lerp(transform.position, _closePos, 3f * Time.deltaTime);
            } else {
                _isMoving = false;
                _open = false;
                _sourceSound.Stop();
            }
        }
    }
}