using UnityEngine;

public class OnOffSwitchDevice : MonoBehaviour {

    #region var
    private bool _on;
    [SerializeField] private Material offColor;
    [SerializeField] private Material onColor;
    [SerializeField] private GameObject target;
    public bool requireKey;
    #endregion

    private void Start() {
        _on = false;
    }

    private void Operate() {

        if (_on) {
            if (requireKey && InventoryManager.Instance.GetItemCount("key") == 0) {
                return;
            }
            GetComponent<Renderer>().material = onColor;
            target.SendMessage("Activate");
            _on = false;

            if (requireKey) {
                InventoryManager.Instance.ConsumeItem("key");
            }

        } else {
            GetComponent<Renderer>().material = offColor;
            target.SendMessage("Deactivate");
            _on = true;
        }
    }
}