
using UnityEngine;

[RequireComponent(typeof(AudioSource))]

public class CollectableItem : MonoBehaviour {

    #region var
    [SerializeField] private string itemName;
    [SerializeField] private int score;
    [SerializeField] private AudioClip fx;
    #endregion

    private void Start() {
        GetComponent<AudioSource>().playOnAwake = false;
        GetComponent<AudioSource>().spatialBlend = 1;
    }

    private void OnTriggerEnter(Collider other) {
        if (other.GetComponent<CharacterController>()) {

            GetComponent<AudioSource>().PlayOneShot(fx);

            Destroy(gameObject);
            InventoryManager.Instance.AddItem(itemName);
            PlayerManager.Instance.CoinPickUp(score);

            if (itemName == "target") {

                PlayerManager.Instance.TargetPickUp();
            }
        }
    }
}