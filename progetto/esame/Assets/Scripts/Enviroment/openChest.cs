﻿using UnityEngine;

public class openChest : MonoBehaviour {
    #region var
    private bool isOpen = false;
    #endregion

    public void Operate() {

        Animator a = GetComponent<Animator>();
        if (!isOpen) {
            //Debug.Log("Chest Open!");
            isOpen = true;
            a.SetBool("isOpen", isOpen);
        } else {
            a.SetBool("catch", true);
        }
    }
}
