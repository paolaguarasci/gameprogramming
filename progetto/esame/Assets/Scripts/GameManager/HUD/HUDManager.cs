﻿using UnityEngine;
using UnityEngine.UI;

public class HUDManager : Singleton<HUDManager> {
    #region var
    [SerializeField] private Text scoreLabel;
    [SerializeField] private Text lifeLabel;
    [SerializeField] private Text nameLabel;
    private string playerName;
    #endregion

    private void Start() {
        PlayerManager.Instance.playerState.AddListener(HandlePlayerStateUpdate);
        GameManager.Instance.OnGameStateChanged.AddListener(HandleGameStateChanged);
        UIManager.Instance.OnMainMenuFadeComplete.AddListener(HandleMainMenuFadeComplete);

        Debug.Log("[HUD MANAGER] INIT Nome: " + playerName);
        OnCollectsCoin(0);
        OnLifeDown(PlayerManager.Instance.life);
        OnSetActive(false);
    }

    private void Update() {
        playerName = PlayerPrefs.GetString("nome");
        if (playerName != null) {
            nameLabel.text = playerName;
        } else {
            nameLabel.text = " - ";
        }
    }

    private void HandlePlayerStateUpdate(float l, int s) {
        OnLifeDown(l);
        OnCollectsCoin(s);
    }

    private void HandleMainMenuFadeComplete(bool fadeOut) {
        if (fadeOut) {
            OnSetActive(true);
        }
    }

    private void HandleGameStateChanged(GameManager.GameState currentState, GameManager.GameState previousState) {
        Debug.LogWarning("[HUD] HandleGameStateChanged " + previousState + " -> " + currentState);
        if (currentState != GameManager.GameState.RUNNING) {
            OnSetActive(false);
        } else {
            OnSetActive(true);
        }


        if (currentState == GameManager.GameState.PREGAME) {

            OnCollectsCoin(0);
            OnLifeDown(PlayerManager.Instance.life);

        }

    }
    public void OnCollectsCoin(int v) {
        scoreLabel.text = v.ToString();

    }

    public void OnLifeDown(float v) {
        lifeLabel.text = v.ToString();
    }

    public void OnSetActive(bool isActive) {
        gameObject.SetActive(isActive);
    }

}
