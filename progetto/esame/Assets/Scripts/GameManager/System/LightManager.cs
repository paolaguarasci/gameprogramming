using UnityEngine;

public class LightManager : Singleton<LightManager> {
    #region var
    [SerializeField] private Light sole;
    #endregion

    public float Luminosita {
        get => sole.intensity;
        set {
            if ((value < 0.5f)) {
                value = 0.5f;
            } else if (value >= 2.5f) {
                value = 2.5f;
            }
            sole.intensity = value;
        }
    }

    public void Start() {
        Debug.Log("Light manager starting...");
        if (sole != null) {
            sole.intensity = 1.5f;
            sole.shadowStrength = 0.3f;
        }
    }

    public void Alza() {
        Debug.Log("Luminosita +0.1f --> " + Luminosita);
        Luminosita += 0.1f;
    }

    public void Abbassa() {
        Debug.Log("Luminosita -0.1f --> " + Luminosita);
        Luminosita -= 0.1f;
    }

}