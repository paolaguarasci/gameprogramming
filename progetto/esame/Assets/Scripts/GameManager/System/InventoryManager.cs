using System.Collections.Generic;
using UnityEngine;

public class InventoryManager : Singleton<InventoryManager> {
    #region var
    private Dictionary<string, int> _items;
    #endregion

    public void Start() {
        Debug.Log("Inventory manager starting...");
        _items = new Dictionary<string, int>();
    }

    public void DisplayItems() {
        string itemDisplay = "List of Items: ";
        foreach (KeyValuePair<string, int> item in _items) {
            itemDisplay += item.Key + " (" + item.Value + ") ";
        }

        //Debug.Log (itemDisplay);
    }

    public void AddItem(string name) {
        if (_items.ContainsKey(name)) {
            _items[name] += 1;

        } else {
            _items[name] = 1;
        }
        DisplayItems();
    }

    public List<string> GetItemsList() {
        List<string> list = new List<string>(_items.Keys);
        return list;
    }
    public int GetItemCount(string name) {
        if (_items.ContainsKey(name)) {
            return _items.Count;
        }
        return 0;
    }

    public void ConsumeItem(string name) {
        if (_items.ContainsKey(name)) {
            _items[name]--;
            if (_items[name] == 0) {
                _items.Remove(name);
            }
        } else {
            //Debug.Log ("Non posso usare " + name);
        }
        DisplayItems();
    }
}