using UnityEngine;

public class PlayerManager : Singleton<PlayerManager> {
    #region var
    private int _score;
    private int _life;
    private string _name;
    private int _healtPackValue;
    public int life { get => _life; private set { } }
    public string playerName { get => _name; private set { } }
    public int score { get => _score; private set { } }
    public int healtPackValue { get => _healtPackValue; private set { } }

    public Events.EventPlayerState playerState;
    #endregion

    private void Start() {
        GameManager.Instance.OnGameStateChanged.AddListener(HandleGameStateChanged);
        initValue();
    }

    private void HandleGameStateChanged(GameManager.GameState currentState, GameManager.GameState previousState) {
        if (currentState == GameManager.GameState.PREGAME) {
            initValue();
        }
        //if (currentState != GameManager.GameState.END && currentState == GameManager.GameState.END) {
        //         PlayerPrefs.SetString("nome", _name);
        //}
    }

    public void LifeDown(int minus) {
        _life -= minus;
        if (_life < 1) {
            GameManager.Instance.GameOver();
            PlayerPrefs.SetInt("score", score);
        }
        playerState.Invoke(life, score);
    }

    public void LifeUp(int plus) {
        _life += plus;
        playerState.Invoke(life, score);
    }

    public void CoinPickUp(int plus) {
        _score += plus;
        playerState.Invoke(life, score);
    }

    public void TargetPickUp() {
        GameManager.Instance.EndGame();
    }

    private void initValue() {
        _healtPackValue = 2;
        _life = 5;
        _score = 0;
        _name = PlayerPrefs.GetString("nome");
        PlayerPrefs.SetInt("score", 0);
    }
}