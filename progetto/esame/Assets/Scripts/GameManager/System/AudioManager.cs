using UnityEngine;

public class AudioManager : Singleton<AudioManager> {
    #region var
    [SerializeField] private AudioSource soundSource;
    [SerializeField] private AudioSource musicSource;
    [SerializeField] private string introBGMusic;
    [SerializeField] private string levelBGMusic;
    [SerializeField] private string pauseBGMusic;
    [SerializeField] private string endBGMusic;
    #endregion

    public float soundVolume {
        get => AudioListener.volume;
        set {
            if ((value < 0f)) {
                value = 0f;
            } else if (value >= 1f) {
                value = 1;
            }
            AudioListener.volume = value;
        }
    }

    private void Start() {
        //Debug.Log("Audio manager starting...");
        soundVolume = 0.35f;
        PlayMusic();
        GameManager.Instance.OnGameStateChanged.AddListener(HandleGameStateChanged);
    }

    private void HandleGameStateChanged(GameManager.GameState currentState, GameManager.GameState previousState) {
        Debug.LogWarning("[AUDIO MANAGER] HandleGameStateChanged " + previousState + " -> " + currentState);
        if (currentState == GameManager.GameState.PREGAME) {
            PlayIntroMusic();
        } else if (currentState == GameManager.GameState.RUNNING) {
            PlayLevelMusic();
        } else if (previousState == GameManager.GameState.RUNNING && currentState == GameManager.GameState.END) {
            PlayEndMusic();
        } else if (previousState != GameManager.GameState.PAUSED && currentState == GameManager.GameState.PAUSED) {
            PlayPauseMusic();
        }
    }


    public bool soundMute {
        get => AudioListener.pause;
        set => AudioListener.pause = value;
    }

    public void PlaySound(AudioClip clip) {
        soundSource.PlayOneShot(clip);
    }

    public void PlayIntroMusic() {

        PlayMusic((AudioClip)Resources.Load("Music/" + introBGMusic));

    }

    public void PlayLevelMusic() {

        PlayMusic((AudioClip)Resources.Load("Music/" + levelBGMusic));

    }
    public void PlayPauseMusic() {

        PlayMusic((AudioClip)Resources.Load("Music/" + pauseBGMusic));

    }

    public void PlayEndMusic() {

        PlayMusic((AudioClip)Resources.Load("Music/" + endBGMusic));

    }
    public void PlayMusic(AudioClip clip) {
        musicSource.clip = clip;
        musicSource.Play();
    }

    public void StopMusic() {
        musicSource.Stop();
    }

    public void StopFX() {
        soundSource.Stop();
    }

    public void PlayMusic() {
        musicSource.clip = (AudioClip)Resources.Load("Music/" + introBGMusic);
        musicSource.Play();
    }

    public void PlayFX() {
        soundSource.Play();
    }

    public void ToggleAll() {
        if (musicSource.isPlaying || soundSource.isPlaying) {
            StopMusic();
            StopFX();
        } else {
            PlayFX();
            PlayMusic();
        }
    }

    public void Alza() {
        Debug.Log("[AUDIO MANAGER] Volume PIU (" + soundVolume + ")");
        soundVolume += 01f;
    }

    public void Abbassa() {
        Debug.Log("[AUDIO MANAGER] Volume MENO (" + soundVolume + ")");
        soundVolume -= 01f;
    }
}