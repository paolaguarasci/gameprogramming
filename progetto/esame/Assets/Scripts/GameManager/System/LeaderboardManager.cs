using Newtonsoft.Json;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LeaderboardManager : Singleton<LeaderboardManager> {
    #region var
    private NetworkService _network;
    public List<Score> scores;
    private string playerName;
    [SerializeField] private GameObject panel;
    [SerializeField] private Text nameLabel;
    [SerializeField] private Text scoreLabel;
    #endregion


    public void Start() {
        GameManager.Instance.OnGameStateChanged.AddListener(HandleGameStateChanged);
        _network = new NetworkService();
        StartCoroutine(_network.GetLeaderboard(OnJSONDataLoaded));
        nameLabel.text = PlayerManager.Instance.playerName;
        scoreLabel.text = PlayerManager.Instance.score.ToString();
    }

    private void Update() {
        playerName = PlayerPrefs.GetString("nome");
        if (playerName != null) {
            nameLabel.text = playerName;
        } else {
            nameLabel.text = " - ";
        }
    }


    private void HandleGameStateChanged(GameManager.GameState currentState, GameManager.GameState previousState) {
        if (currentState == GameManager.GameState.END) {
            Debug.Log("[LEADERBOARD] ok");
            LogScore(PlayerManager.Instance.playerName, PlayerManager.Instance.score);
            StartCoroutine(_network.GetLeaderboard(OnJSONDataLoaded));
        }
    }

    public void LogScore(string name, int score) {
        StartCoroutine(_network.SavePoint(name, score, OnLogged));
    }

    private void OnLogged(string res) {
        //Debug.Log("RESPONSE: " + res);
    }

    public void OnJSONDataLoaded(string data) {
        Debug.Log("Inizio il parsing della leaderboard");

        if (panel != null) {
            List<Score> scores = JsonConvert.DeserializeObject<List<Score>>(data);
            int k = 1;
            foreach (Score s in scores) {
                GameObject tmp = new GameObject();
                Shadow shadow = tmp.AddComponent<Shadow>();
                RectTransform rc = tmp.AddComponent<RectTransform>();
                Text txt = tmp.AddComponent<Text>();

                tmp.name = "Leader1";
                tmp.transform.SetParent(panel.transform);

                rc.sizeDelta = new Vector2(100, 200);
                rc.localPosition = new Vector3(0, 0, 0);

                txt.fontSize = 12;
                // txt.font = Resources.GetBuiltinResource (typeof (Font), "Arial.ttf") as Font;
                txt.font = Resources.Load<Font>("Fonts/Kenney_Future");

                txt.text = k.ToString() + ". ";
                txt.text += s.name;
                txt.text += " (" + s.score + " pts)";
                k++;
                Debug.Log("NOME " + s.name);
            }
            k = 1;
        }
        Debug.Log("Finisco il parsing della leaderboard");

    }
}