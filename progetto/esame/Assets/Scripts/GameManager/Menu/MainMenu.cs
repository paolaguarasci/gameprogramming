﻿using UnityEngine;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour {
    #region var
    [SerializeField] private Animation _mainMenuAnimator;
    [SerializeField] private AnimationClip _fadeInAnimation;
    [SerializeField] private AnimationClip _fadeOutAnimation;
    [SerializeField] private InputField nameField;
    private string playerName;

    public Events.EventFadeComplete OnMainMenuFadeComplete;
    #endregion

    private void Start() {
        GameManager.Instance.OnGameStateChanged.AddListener(HandleGameStateChanged);
        PlayerPrefs.SetInt("score", 0);
        playerName = PlayerPrefs.GetString("nome");
        nameField.text = playerName;
    }

    private void Update() {
        // PlayerPrefs.SetString("nome", nameField.text);
    }

    public void OnClickClose() {
        GameManager.Instance.QuitGame();
    }

    public void FadeIn() {
        // OnMainMenuFadeComplete.Invoke(false);
        Debug.LogWarning("[Main Menu] FadeIn...");
        UIManager.Instance.SetDummyCameraActive(true);
        _mainMenuAnimator.Stop();
        _mainMenuAnimator.clip = _fadeInAnimation;
        _mainMenuAnimator.Play();

    }
    public void OnFadeInComplete() {
        OnMainMenuFadeComplete.Invoke(false);
    }

    public void FadeOut() {
        // OnMainMenuFadeComplete.Invoke(false);
        Debug.LogWarning("[Main Menu] FadeOut...");
        UIManager.Instance.SetDummyCameraActive(false);
        _mainMenuAnimator.Stop();
        _mainMenuAnimator.clip = _fadeOutAnimation;
        _mainMenuAnimator.Play();
    }
    public void OnFadeOutComplete() {
        OnMainMenuFadeComplete.Invoke(true);
    }

    private void HandleGameStateChanged(GameManager.GameState currentState, GameManager.GameState previousState) {
        Debug.LogWarning("[Main Menu] HandleGameStateChanged " + previousState + " -> " + currentState);
        if (previousState == GameManager.GameState.PREGAME && currentState == GameManager.GameState.RUNNING) {
            FadeOut();
            PlayerPrefs.SetString("nome", nameField.text);
        }

        if (previousState != GameManager.GameState.PREGAME && currentState == GameManager.GameState.PREGAME) {
            FadeIn();
        }
    }
}