using UnityEngine;

public class PauseMenu : MonoBehaviour {
    #region var
    // [SerializeField] private Text nameLabel  = null ;
    [SerializeField] private AudioClip sound;
    #endregion

    public void OnExit() {
        AudioManager.Instance.PlaySound(sound);
        GameManager.Instance.EndGame();
    }
    public void OnVolumePiu() {
        AudioManager.Instance.PlaySound(sound);
        AudioManager.Instance.Alza();
    }
    public void OnVolumeMeno() {
        AudioManager.Instance.PlaySound(sound);
        AudioManager.Instance.Abbassa();
    }
    public void OnLuminositaPiu() {
        AudioManager.Instance.PlaySound(sound);
        LightManager.Instance.Alza();
    }
    public void OnLuminositaMeno() {
        AudioManager.Instance.PlaySound(sound);
        LightManager.Instance.Abbassa();

    }

}