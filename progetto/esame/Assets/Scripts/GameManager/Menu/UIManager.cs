﻿using UnityEngine;

public class UIManager : Singleton<UIManager> {
    #region var
    [SerializeField] private MainMenu _mainMenu;
    [SerializeField] private PauseMenu _pauseMenu;
    [SerializeField] private Camera _dummyCamera;
    [SerializeField] private Texture2D cursorArrow;
    [SerializeField] private GameOverScreen _gameOver;

    public Events.EventFadeComplete OnMainMenuFadeComplete;
    public Events.EventFadeComplete OnGameOverFadeComplete;
    #endregion

    private void Start() {
        GameManager.Instance.OnGameStateChanged.AddListener(HandleGameStateChanged);
        _mainMenu.OnMainMenuFadeComplete.AddListener(HandleMainMenuFadeComplete);
        _gameOver.OnGameOverFadeComplete.AddListener(HandleGameOverFadeComplete);
        Cursor.SetCursor(cursorArrow, Vector2.zero, CursorMode.ForceSoftware);
    }

    private void Update() {
        if (GameManager.Instance.CurrentGameState == GameManager.GameState.RUNNING) {
            SetDummyCameraActive(false);
        }

        if (GameManager.Instance.CurrentGameState != GameManager.GameState.PREGAME) {
            return;
        }


        if (Input.GetKeyDown(KeyCode.Space)) {
            if (_mainMenu != null) {
                GameManager.Instance.StartGame();
            }
        }
    }

    private void HandleGameStateChanged(GameManager.GameState currentState, GameManager.GameState previousState) {
        SetPauseMenuActive(currentState == GameManager.GameState.PAUSED);
        SetGameOverActive(currentState == GameManager.GameState.GAMEOVER);
    }

    public void HandleMainMenuFadeComplete(bool isActive) {
        OnMainMenuFadeComplete.Invoke(isActive);
    }

    public void HandleGameOverFadeComplete(bool isActive) {
        OnGameOverFadeComplete.Invoke(true);
    }

    public void SetDummyCameraActive(bool isActive) {
        _dummyCamera.gameObject.SetActive(isActive);
    }

    public void SetPauseMenuActive(bool isActive) {
        Debug.Log("SetPauseMenuActive");
        _pauseMenu.gameObject.SetActive(isActive);
    }

    private void SetGameOverActive(bool isActive) {
        _gameOver.gameObject.SetActive(isActive);
    }

    public void RestartFromEnd() {
        _mainMenu.FadeIn();
    }

}