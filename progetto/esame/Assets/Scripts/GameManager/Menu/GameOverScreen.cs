﻿using UnityEngine;

public class GameOverScreen : MonoBehaviour {

    #region var
    [SerializeField] private Animation _gameOverAnimator;
    [SerializeField] private AnimationClip _fadeInAnimation;

    public Events.EventFadeComplete OnGameOverFadeComplete;
    #endregion

    private void Start() {
        FadeIn();
        GameManager.Instance.OnGameStateChanged.AddListener(HandleGameStateChanged);
    }

    private void HandleGameStateChanged(GameManager.GameState currentState, GameManager.GameState previousState) {
        Debug.LogWarning("[GAME OVER SCREEN] HandleGameStateChanged " + previousState + " -> " + currentState);
        if (previousState == GameManager.GameState.RUNNING && currentState == GameManager.GameState.GAMEOVER) {
            Debug.LogWarning("[GAME OVER SCREEN] Animazione triggerata, avvio...");
        }
    }

    public void FadeIn() {
        Debug.LogWarning("[GAME OVER SCREEN] Animazione triggerata, avvio...");
        _gameOverAnimator.Stop();
        _gameOverAnimator.clip = _fadeInAnimation;
        _gameOverAnimator.Play();
        Debug.LogWarning("[GAME OVER SCREEN] Animazione triggerata, fine ");
    }

    public void OnFadeInComplete() {
        Debug.LogWarning("[GAME OVER SCREEN] Animazione triggerata, fine, entro nella callback");
        OnGameOverFadeComplete.Invoke(true);
        Debug.LogWarning("[GAME OVER SCREEN] Animazione triggerata, fine, esco nella callback");
    }
}
