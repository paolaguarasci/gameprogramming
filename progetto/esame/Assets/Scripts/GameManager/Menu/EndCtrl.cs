using UnityEngine;
using UnityEngine.UI;

public class EndCtrl : MonoBehaviour {
    #region var
    [SerializeField] private Text scoreLabel;
    [SerializeField] private Text nameLabel;
    [SerializeField] private Texture2D cursorArrow;
    #endregion

    // Start is called before the first frame update
    private void Start() {
        scoreLabel.text = PlayerPrefs.GetInt("score").ToString();
        nameLabel.text = PlayerPrefs.GetString("nome");
        Cursor.SetCursor(cursorArrow, Vector2.zero, CursorMode.ForceSoftware);
    }

    // Update is called once per frame
    private void Update() {

    }
}