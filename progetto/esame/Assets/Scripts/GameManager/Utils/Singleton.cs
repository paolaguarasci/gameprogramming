﻿using UnityEngine;


// SINGLETON /////////////////////////////////////////////
// Il GameManager e' un singleton
// Come si implementa in C#? Istanza statica privata e Awake che controlla se unico altrimenti distrugge

/* 
    private static GameManager instance;
    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
            Debug.LogError("[GameManager] Multipla istanza");
        }
    }
*/
// SINGLETON /////////////////////////////////////////////


// where T : Singleton<T> ==> Significa che T deve estendere Singleton...
public class Singleton<T> : MonoBehaviour where T : Singleton<T> {
    private static T instance;

    public static T Instance => instance;


    public static bool IsInitialized => instance != null;


    protected virtual void Awake() {
        if (instance != null) {
            Debug.LogError("[SINGLETON] Attenzione! Stai pronvando ad instanziare due volte lo stesso singleton " + typeof(T));
        } else {
            instance = (T)this;
        }
    }

    protected virtual void OnDestroy() {
        if (instance == this) {
            instance = null;
        }
    }


}
