﻿using UnityEngine.Events;



public class Events {
    // il primo e' lo stato attuale e il secondo e' il nuovo stato
    [System.Serializable] public class EventGameState : UnityEvent<GameManager.GameState, GameManager.GameState> { }
    [System.Serializable] public class EventFadeComplete : UnityEvent<bool> { }
    [System.Serializable] public class EventPlayerState : UnityEvent<float, int> { }
    [System.Serializable] public class EventSpeedChange : UnityEvent<float> { }
}
