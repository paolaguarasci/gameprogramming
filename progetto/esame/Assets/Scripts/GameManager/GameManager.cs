﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : Singleton<GameManager> {
    // Tengo traccia di:
    //      Livello attualmente caricato
    //      Stato del gioco
    // 
    // Genero:
    //      altri sistemi persistenti


    // Stati del gioco
    // PREGAME, RUNNING, PAUSED
    #region var
    public enum GameState {
        PREGAME,
        RUNNING,
        PAUSED,
        GAMEOVER,
        END
    }

    public GameObject[] SystemPrefabs;
    private List<GameObject> _instancedSystemPrefabs;
    private string _currentLevelName = string.Empty;
    private List<AsyncOperation> _loadOperations;
    private GameState _currentGameState = GameState.PREGAME;
    public Events.EventGameState OnGameStateChanged;
    public Events.EventSpeedChange OnSpeedChanged;
    public GameState CurrentGameState { get => _currentGameState; private set => _currentGameState = value; }
    #endregion

    private void Start() {
        DontDestroyOnLoad(gameObject);

        _loadOperations = new List<AsyncOperation>();
        _instancedSystemPrefabs = new List<GameObject>();

        InstantiateSystemPrefabs();

        UIManager.Instance.OnMainMenuFadeComplete.AddListener(HandleMainMenuFadeComplete);
        UIManager.Instance.OnGameOverFadeComplete.AddListener(HandleGameOverFadeComplete);

    }

    private void Update() {
        if (Input.GetKeyDown(KeyCode.Escape)) {
            if ((_currentGameState == GameState.RUNNING)) TogglePause();
            else if ((_currentGameState == GameState.PAUSED)) TogglePause();
            else if ((_currentGameState == GameState.END)) RestartGame();
            else if ((_currentGameState == GameState.GAMEOVER)) EndGame();
        }
    }

    private void OnLoadOpearateComplete(AsyncOperation ao) {
        if (_loadOperations.Contains(ao)) {
            _loadOperations.Remove(ao);

            // controllo se ho finito di caricare
            if (_currentLevelName == "Game") {
                UpdateState(GameState.RUNNING);
            }

        }
        Debug.Log("[GameManager] Load scena...");
    }

    private void OnUnLoadOpearateComplete(AsyncOperation a) {
        Debug.Log("[GameManager] Unload scena...");
    }

    private void UpdateState(GameState state) {
        GameState previousGameState = _currentGameState;
        _currentGameState = state;
        Debug.Log("Stato previousGameState " + previousGameState + " Stato corrente " + state);

        switch (_currentGameState) {
            case GameState.PREGAME:
                Time.timeScale = 1.0f;
                PlayerManager.Instance.LifeUp(-PlayerManager.Instance.life + 5);
                break;
            case GameState.RUNNING:
                if (previousGameState == GameState.END) return;
                Time.timeScale = 1.0f;
                break;
            case GameState.PAUSED:
                Time.timeScale = 0.0f;
                break;
            case GameState.GAMEOVER:
                Time.timeScale = 0.0f;
                break;
            case GameState.END:
                Time.timeScale = 0.0f;
                UnloadLevel(_currentLevelName);
                LoadLevel("End");
                break;
            default:
                break;
        }

        // dispatch message
        // transition between scene
        // ... etc ...

        // dispatch message
        OnGameStateChanged.Invoke(_currentGameState, previousGameState);
    }

    private void InstantiateSystemPrefabs() {
        GameObject prefabInstance;
        for (int i = 0; i < SystemPrefabs.Length; ++i) {
            prefabInstance = Instantiate(SystemPrefabs[i]);
            _instancedSystemPrefabs.Add(prefabInstance);
        }
    }

    public void LoadLevel(string levelName) {
        // LoadSceneMode.Additive non fa l'unload automatico della scena attuale
        AsyncOperation ao = SceneManager.LoadSceneAsync(levelName, LoadSceneMode.Additive);
        // esco se ho problemi 
        if (ao == null) {
            Debug.LogError("[GameManager] Errore nel caricamento della scena " + levelName);
            return;
        }

        // "Mi" registro all'evento "completed" e indico la callback
        ao.completed += OnLoadOpearateComplete;
        _loadOperations.Add(ao);
        _currentLevelName = levelName;
    }

    public void UnloadLevel(string levelName) {
        AsyncOperation ao = SceneManager.UnloadSceneAsync(levelName);
        // esco se ho problemi 
        if (ao == null) {
            Debug.LogError("[GameManager] Errore nell'unload della scena " + levelName);
            return;
        }
        // "Mi" registro all'evento "completed" e indico la callback
        ao.completed += OnUnLoadOpearateComplete;
        _currentLevelName = string.Empty;
    }


    // Overload del distruttore
    // per distruzione "profonda" di ogni manager
    protected override void OnDestroy() {
        // Richiamo il distruttore di base
        base.OnDestroy();

        for (int i = 0; i < _instancedSystemPrefabs.Count; ++i) {
            Destroy(_instancedSystemPrefabs[i]);
        }
        _instancedSystemPrefabs.Clear();
    }

    public void StartGame() {
        LoadLevel("Game");
    }


    public void TogglePause() {
        UpdateState(_currentGameState == GameState.RUNNING ? GameState.PAUSED : GameState.RUNNING);
    }


    public void RestartGame() {
        Debug.Log("[Game Manager] Restart game...");

        UpdateState(GameState.PREGAME);
    }
    
    public void GameOver() {
        Debug.Log("[Game Manager] GAMEOVER...");
        UpdateState(GameState.GAMEOVER);
    }
    public void EndGame() {
        Debug.Log("[Game Manager] End game...");
        UpdateState(GameState.END);

    }

    public void QuitGame() {
        Debug.Log("[Game Manager] Quit game...");

        // implement features for quitting...
        // autosave... etc etc

        Application.Quit();
    }


    private void HandleMainMenuFadeComplete(bool fadeOut) {
        if (!fadeOut) {
            UnloadLevel(_currentLevelName);
        }
    }


    private void HandleGameOverFadeComplete(bool fadeOut) {
        
        EndGame();
    }

    private void HandleSpeedChange(float newSpeedValue) {
        OnSpeedChanged.Invoke(newSpeedValue);
    }

}

