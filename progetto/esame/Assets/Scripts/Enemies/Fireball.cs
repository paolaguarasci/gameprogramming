using UnityEngine;

public class Fireball : MonoBehaviour {
    #region Variabili
    private float speed = 25.0f;
    public int damage = 1;
    public float maxDist = 15f;
    private Vector3 posIniziale;
    private bool _giaColpito;
    #endregion
    private void Start() {
        _giaColpito = false;
        transform.position = new Vector3(transform.position.x, 1.4f, transform.position.z);
        posIniziale = transform.position;
    }

    private void Update() {
        float dist = Vector3.Distance(posIniziale, transform.position);
        if (dist <= maxDist) {
            transform.Translate(0, 0, speed * Time.deltaTime);
        } else {
            Destroy(gameObject);
        }
    }

    private void OnTriggerEnter(Collider other) {
        PlayerCharacter player = other.GetComponent<PlayerCharacter>();
        if (player != null && !_giaColpito) {
            //Debug.Log("Player HIT");
            player.Hurt(damage);
            _giaColpito = true;
        }
        Destroy(gameObject);
    }
}