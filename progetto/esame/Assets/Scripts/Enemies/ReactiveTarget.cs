using System.Collections;
using UnityEngine;

public class ReactiveTarget : MonoBehaviour {
    private bool live = true;
    private int life = 2;
    [SerializeField] private Animator _animator;
    [SerializeField] private AudioClip _hitSound;
    [SerializeField] private AudioSource _sourceAudio;
    public void reactToHit() {
        
        if (live) {

            _sourceAudio.PlayOneShot(_hitSound);

            if (life > 0) {
                StartCoroutine(Hitted());
            } else {
                StartCoroutine(Died());
            }
        }
    }

    private IEnumerator Died() {
        
        live = false;
        Debug.Log("Die, life: " + life);
        Vector3 scaleChange = new Vector3(0, -0.2f, 0);
        Vector3 positionChange = new Vector3(0, -0.2f, 0);
        transform.localScale += scaleChange;
        transform.position += positionChange;
        yield return new WaitForSeconds(1f);
        Destroy(gameObject);
        PlayerManager.Instance.CoinPickUp(300);
    }

    private IEnumerator Hitted() {
        Debug.Log("Hit, life: " + life);
        life -= 1;

        Vector3 scaleChange = new Vector3(0, -0.2f, 0);
        Vector3 positionChange = new Vector3(0, -0.2f, 0);
        transform.localScale += scaleChange;
        transform.position += positionChange;
        _animator.speed = 0f;
        yield return new WaitForSeconds(1f);
        _animator.speed = 1f;

        Vector3 revertScaleChange = new Vector3(0, +0.2f, 0);
        Vector3 revertPositionChange = new Vector3(0, +0.2f, 0);
        transform.localScale += revertScaleChange;
        transform.position += revertPositionChange;
        PlayerManager.Instance.CoinPickUp(100);
    }
}