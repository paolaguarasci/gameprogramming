using UnityEngine;

public class Spin : MonoBehaviour {
    #region var
    public float speed = 3.0f;
    #endregion
    private void Update() {
        transform.Rotate(0, speed, 0);
    }
}