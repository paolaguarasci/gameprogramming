using UnityEngine;

public class GallinaAI : MonoBehaviour {
    #region var
    private bool _alive = true;
    public float speed = 3.0f;
    private float baseSpeed = 3.0f;
    public float obstacleRange = 5.0f;
    private bool insegui = false;
    private GameObject _fireball = null;
    private Vector3 posIniziale;
    private float distanzaDiFuga;
    private float distanzaDiSparo;
    [SerializeField] private GameObject fireballPrefab;
    [SerializeField] private float distanza = 0f;
    // [SerializeField] private GameObject player = null;
    private PlayerCharacter pc;
    #endregion

    // Start is called before the first frame update
    private void Start() {
        posIniziale = transform.position;
        // Debug.Log("Pos Iniziale Y" + posIniziale.y);
        distanza = 7f;
        _alive = true;
        insegui = false;
        distanzaDiFuga = 10f;
        pc = null;
        distanzaDiSparo = 25f;
    }

    private void Awake() {
        GameManager.Instance.OnSpeedChanged.AddListener(OnSpeedChanged);
    }

    private void OnSpeedChanged(float value) {
        speed = baseSpeed * value;
        //Debug.Log("Enemy Speed: " + speed);
    }

    private void Update() {
        if (_alive) // si muove solo se vivo!!
        {
            Ray ray = new Ray(transform.position, transform.forward);

            if (Physics.SphereCast(ray, 0.75f, out RaycastHit hit)) {
                GameObject hitObject = hit.transform.gameObject;
                if (hitObject.GetComponent<PlayerCharacter>() && hit.distance < distanzaDiFuga) {
                    insegui = true;
                    pc = hitObject.GetComponent<PlayerCharacter>();
                } else if (insegui && hitObject.GetComponent<PlayerCharacter>() && hit.distance > distanzaDiFuga) {
                    insegui = false;
                    posIniziale = transform.position;
                    pc = null;
                }

                if (hitObject.GetComponent<PlayerCharacter>() && hit.distance < distanzaDiSparo) {
                    Spara();
                }
            }
            if (insegui && pc != null) {
                //Debug.Log("Posizione PLAYER (" + pc.transform.position.x + ", " + pc.transform.position.y + ", " + pc.transform.position.z + ")");

                Vector3 filteredPlayerPosition = new Vector3(pc.transform.position.x, transform.position.y, pc.transform.position.z);

                transform.position = Vector3.MoveTowards(transform.position, filteredPlayerPosition, speed * Time.deltaTime);
                transform.LookAt(filteredPlayerPosition, Vector3.up);
                Spara();
            } else {
                transform.Translate(0, 0, speed * Time.deltaTime);
                float dist = Vector3.Distance(posIniziale, transform.position);
                if (dist > distanza) {
                    float angle = 180;
                    transform.Rotate(0, angle, 0);
                }

            }

        }
    }
    private void Spara() {
        if (_fireball == null) {
            _fireball = Instantiate(fireballPrefab);
            _fireball.transform.position = transform.TransformPoint(Vector3.forward * 1.5f);
            _fireball.transform.rotation = transform.rotation;
        }
    }
    public bool isAlive() { return _alive; }
    public void SetAlive(bool alive) {
        _alive = alive;
    }
}