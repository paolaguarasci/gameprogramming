using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Networking;

public class NetworkService {

    #region Common
    /**
    * 
    * Generic CALL API
    * @param url, args, callaback
    * 
    **/

    private IEnumerator CallAPI(string url, Hashtable args, Action<string> callback) {
        UnityWebRequest www;
        if (args == null) {
            www = UnityWebRequest.Get(url);
        } else {
            WWWForm form = new WWWForm();
            foreach (DictionaryEntry arg in args) {
                form.AddField(arg.Key.ToString(), arg.Value.ToString());
            }
            www = UnityWebRequest.Post(url, form);
        }
        yield return www.SendWebRequest();
        if (IsResponseValid(www)) {
            callback(www.downloadHandler.text);
        }
    }

    private bool IsResponseValid(UnityWebRequest www) {
        if (www.error != null) {
            //Debug.Log("Errore di connessione");
            return false;
        } else if (string.IsNullOrEmpty(www.downloadHandler.text)) {
            //Debug.Log("Errore nei dati");
            return false;
        }
        return true;
    }
    #endregion

    #region IMAGE

    /**
    * 
    * Download IMG
    * @param url, callaback
    * 
    **/

    public IEnumerator DownloadImage(string url, Action<Texture2D> callback) {
        //Debug.Log("DownloadImage");
        UnityWebRequest www = UnityWebRequestTexture.GetTexture(url);
        yield return www.SendWebRequest();
        Texture2D dowloadedTexture = DownloadHandlerTexture.GetContent(www);
        callback(dowloadedTexture);
    }
    #endregion

    #region LEADERBOARD
    /**
     * 
     * LEADERBOARD
     * 
     * 
     **/


    //private string saveLocalServer = "http://localhost:3000/save";
    //private string getLocalServer = "http://localhost:3000/best";

    private string saveServer = "https://tranquil-reaches-15173.herokuapp.com/save";
    private string getServer = "https://tranquil-reaches-15173.herokuapp.com/best";

    public IEnumerator SavePoint(string name, int score, Action<string> callback) {
        Hashtable args = new Hashtable {
            { "name", name },
            { "score", score },
            { "timestamp", DateTime.UtcNow }
        };
        return CallAPI(saveServer, args, callback);
    }

    public IEnumerator GetLeaderboard(Action<string> callback) {
        //Debug.Log("Avvio il download della leaderboard");
        return CallAPI(getServer, null, callback);
    }
    #endregion
}